const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
const dialog = electron.dialog
// App menu
const Menu = electron.Menu
// IPC for main/renderer communication
const ipc = electron.ipcMain
// Other stuff
const path = require('path')
const url = require('url')
const fs = require('fs');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

// Check for app updates
const autoUpdater = electron.autoUpdater;
const appVersion = require('./package.json').version;
const os = require('os').platform();
var feedUrl = 'http://triwou.org/api/v1/appupdate/tnf?version=' + app.getVersion();
autoUpdater.setFeedURL(feedUrl);

// Event handling after download new release
autoUpdater.on('update-downloaded', function (event, releaseNotes, releaseName, releaseDate, updateUrl, quitAndUpdate)
{
  // Confirm install or not to user
  var index = dialog.showMessageBox(mainWindow, {
    type: 'info',
    buttons: [i18n.__('Restart'), i18n.__('Later')],
    title: "TNF Activity Log",
    message: i18n.__('The new version has been downloaded. Please restart the application to apply the updates.'),
    detail: releaseName + "\n\n" + releaseNotes
  });

  if (index === 1) {
    return;
  }

  // Restart app, then update will be applied
  autoUpdater.quitAndUpdate();
});

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    title: 'TNF Activity Log',
    frame: true,
    resizable: true,
    transparent: false
  })

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'www/index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Open the DevTools.
  mainWindow.webContents.openDevTools()

  // Create the app menu
  const menuTemplate = [{
    label: 'File',
    submenu: [
      {
        label: 'Print',
        accelerator: 'CmdOrCtrl+P',
        click: () => {
          printWindow();
        }
      },
      {
        label: 'Save as PDF',
        accelerator: 'CmdOrCtrl+D',
        click: () => {
          savePDFWindow();
        }
      },
      {
        type: 'separator'
      }, {
        label: 'Quit',
        accelerator: 'CmdOrCtrl+Q',
        click: () => {
          app.quit();
        }
      }
    ]
  }];
  const menu = Menu.buildFromTemplate(menuTemplate);
  Menu.setApplicationMenu(menu);

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

function printWindow()
{
  var options = {
    printBackground: true
  };
  mainWindow.webContents.print(options);
}

function savePDFWindow()
{
  var options = {
    landscape: false,
    marginsType: 0,
    printBackground: true,
    printSelectionOnly: false,
    pageSize: 'A4',
  };

  var saveOptions = {
    defaultPath: 'TNFApp.pdf'
  };

  dialog.showSaveDialog(mainWindow, saveOptions, function( file_path ) {
    mainWindow.webContents.printToPDF( options, function(error, data) {
      if (error) throw error
      fs.writeFile(file_path, data, (error) => {
        if (error) throw error
        console.log('Write PDF successfully.')
      })
    });
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
