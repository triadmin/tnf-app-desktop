import { Component, ViewChild } from '@angular/core';
import { ViewController, NavParams, Events } from 'ionic-angular';
import { GoogleAnalytics } from 'ionic-native';

import { Constants } from '../../../providers/config/config';
import { Keyboard } from 'ionic-native';

// Services
import { SurveyService } from '../../../providers/survey-service/survey-service';
import { HelperService } from '../../../providers/helper-service/helper-service';

@Component({
  templateUrl: 'notifications-modal.html'
})

export class NotificationsModalPage
{
  notification: string = '';
  surveyAttempt: any;
  surveyAttemptTitle: string = '[No Title]';
  metaData: any;
  user: any = Constants.USER;
  tnf_users: any = Constants.TNF_USERS;
  recipient: string;
  recipientId: number;

  constructor(
    private viewCtrl: ViewController,
    private params: NavParams,
    private surveyService: SurveyService,
    private helperService: HelperService,
    private events: Events
  ) {
    this.surveyAttempt = params.get( 'surveyAttempt' );
    this.metaData = params.get('metaData');


    if ( this.surveyAttempt.SurveyAttemptsTitle !== '' )
    {
      this.surveyAttemptTitle = this.surveyAttempt.SurveyAttemptsTitle;
    }

    this.determineRecipient();

    GoogleAnalytics.trackView('Send Notification');
  }

  determineRecipient()
  {
    // Am I a TNF admin?
    if ( this.user.UsersRole === 'Survey Report Admin' )
    {

      // Then this notification is being sent to the TNF.
      this.recipient = this.surveyAttempt.User.UsersFirstName + ' ' + this.surveyAttempt.User.UsersLastName;
      this.recipientId = this.surveyAttempt.SurveyAttemptsUserId;
    } else {
      // Then this notification is being sent to TNF admins.
      this.recipient = 'your TNF Administrator';
      for ( let tnf in this.tnf_users )
      {
        if ( this.tnf_users[tnf].UsersRole === 'Survey Report Admin' )
        {
          this.recipientId = this.tnf_users[tnf].UsersId;
        }
      }
    }
  }

  sendNotification()
  {
    let surveyMetaDataEntryId = this.helperService.createRandomString(15);
    let data = {
      SurveyMetaDataEntryId: surveyMetaDataEntryId,
      SurveyMetaDataSurveyId: this.surveyAttempt.SurveyAttemptsSurveyId,
      SurveyMetaDataSurveyAttemptId: this.surveyAttempt.SurveyAttemptsId,
      SurveyMetaDataEntryType: 'notification',
      SurveyMetaDataKey: 'notification',
      SurveyMetaDataValue: this.notification
    };

    let recipData = {
      SurveyMetaDataEntryId: surveyMetaDataEntryId,
      SurveyMetaDataSurveyId: this.surveyAttempt.SurveyAttemptsSurveyId,
      SurveyMetaDataSurveyAttemptId: this.surveyAttempt.SurveyAttemptsId,
      SurveyMetaDataEntryType: 'notification',
      SurveyMetaDataKey: 'notification_recipient',
      SurveyMetaDataValue: this.recipientId
    }

    let fromData = {
      SurveyMetaDataEntryId: surveyMetaDataEntryId,
      SurveyMetaDataSurveyId: this.surveyAttempt.SurveyAttemptsSurveyId,
      SurveyMetaDataSurveyAttemptId: this.surveyAttempt.SurveyAttemptsId,
      SurveyMetaDataEntryType: 'notification',
      SurveyMetaDataKey: 'notification_from',
      SurveyMetaDataValue: this.user.UsersId
    }

    let readData = {
      SurveyMetaDataEntryId: surveyMetaDataEntryId,
      SurveyMetaDataSurveyId: this.surveyAttempt.SurveyAttemptsSurveyId,
      SurveyMetaDataSurveyAttemptId: this.surveyAttempt.SurveyAttemptsId,
      SurveyMetaDataEntryType: 'notification',
      SurveyMetaDataKey: 'notification_read',
      SurveyMetaDataValue: 'No'
    }

    if ( this.notification.length > 0 )
    {
      // Save notification
      this.surveyService.saveSurveyMetaData( data ).subscribe(data => {
        this.events.publish('app:log', {
          message: 'New notification has been saved.',
          status: 'success',
          type: 'save notification'
        });
      });

      // Save recipient
      this.surveyService.saveSurveyMetaData( recipData ).subscribe(data => {});

      // Save author (notification from)
      this.surveyService.saveSurveyMetaData( fromData ).subscribe(data => {});

      // Save read data
      this.surveyService.saveSurveyMetaData( readData ).subscribe(data => {});

      // Send notification
      this.surveyService.sendSurveyNotification( this.surveyAttempt.SurveyAttemptsId, this.recipientId, this.user.UsersId ).subscribe(data => {
        this.events.publish('activity:updateactivitydata', this.surveyAttempt.SurveyAttemptsId);
      });
    }

    this.dismissModal();
  }

  dismissModal() {
    Keyboard.close();
    this.viewCtrl.dismiss();
  }
}
