import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular';
import { Constants } from '../../../providers/config/config';
import { SurveyService } from '../../../providers/survey-service/survey-service';
import { ToDosService } from '../../../providers/todos-service/todos-service';
import { HelperService } from '../../../providers/helper-service/helper-service';

@Component({
  selector: 'todos-dashboard',
  templateUrl: 'todos-dashboard.component.html'
})
export class ToDosDashboardComponent {
  todos: any = [];

  constructor(
    private surveyService: SurveyService,
    private helperService: HelperService,
    private todosService: ToDosService,
    private events: Events
  ) {
  }

  deleteToDo( todo )
  {
    this.todosService.deleteToDo( todo.entryId ).subscribe(data => {
      // Remove this item from the list
      this.todos.splice( this.todos.indexOf(todo), 1 );
    });
  }

  toggleToDoComplete( todo )
  {
    this.todosService.toggleToDoComplete( todo.entryId, todo.todos_status ).subscribe(data => {
      if ( todo.todos_status === 'complete' )
      {
        todo.todos_status = 'incomplete';
      } else {
        todo.todos_status = 'complete';
      }
    });
  }

  showActivityPage( activityId )
  {
    this.surveyService.getSurveyAttempt( activityId ).subscribe(data => {
      let activity = data.json().data;
      this.events.publish('activity:view', activity);
    });
  }

  refreshToDoList()
  {
    this.todosService.getAllToDos('incomplete').subscribe(data => {
      this.todos = data.json().data;
    });
  }
}
