import { Component } from '@angular/core';
import { Events, AlertController, ActionSheetController } from 'ionic-angular';
import { Constants } from '../../../providers/config/config';
import { SurveyService } from '../../../providers/survey-service/survey-service';
import { HelperService } from '../../../providers/helper-service/helper-service';

@Component({
  selector: 'activity-log-list',
  templateUrl: 'activity-list.component.html'
})
export class ActivityListComponent {
  message: string;
  entries: any = [];
  // Make a copy of our entries to restore list after search.
  entriesCopy: any = [];
  entriesLoaded: boolean = false;
  showIntroPanel: boolean = false;
  searchResults: boolean = true;
  rowColor: string = '#ffffff';
  platformWidth: number = Constants.PLATFORM_WIDTH;
  getOffset: number = 0;

  constructor(
    private surveyService: SurveyService,
    private helperService: HelperService,
    private alertCtrl: AlertController,
    private events: Events,
    private actionSheetCtrl: ActionSheetController
  ) {
    this.surveyService = surveyService;

    // After editing survey, we need to render updated data.
    this.events.subscribe('activity:updateactivitydata', ( survey_attempt_id ) => {
      this.refreshActivityList( survey_attempt_id );
    });

    // When deleting an activity from its view page, we need to
    // remove it from the activity list.
    this.events.subscribe('activity:removefromlist', ( surveyAttemptId ) => {
      this.removeActivityFromList( surveyAttemptId );
    });
  }

  getLogEntries( infiniteScrollEvent )
  {
    this.surveyService.getSurveyAttempts('Yes', this.getOffset, 20).subscribe(data => {
      let results = data.json().data;
      this.getOffset = this.getOffset + 20;

      this.makeEntriesList( results );
      this.entriesLoaded = true;

      if ( this.entries.length === 0 )
      {
        this.showIntroPanel = true;
      } else {
        // Make a copy of our entries to restore list after search.
        for ( let item in this.entries )
        {
          this.entriesCopy.push( this.entries[item] );
        }
      }

      // Infinite scrolling.
      if ( infiniteScrollEvent !== null )
      {
        infiniteScrollEvent.complete();
      }
    });
  }

  searchLogEntries( ev )
  {
    let search_term = ev.target.value;

    if ( search_term && search_term.length > 3 )
    {
      // Clear this.entries array to prep for search results.
      this.entries.length = 0;

      this.surveyService.searchSurveyAttempts(search_term, 'Yes').subscribe(data => {
        this.searchResults = true;
        let results = data.json().data;

        this.makeEntriesList( results );

        if ( this.entries.length === 0 )
        {
          this.searchResults = false;
        }
      });
    } else {
      // Restore original entry list.
      this.entries.length = 0;

      for ( var item in this.entriesCopy )
      {
        this.entries.push( this.entriesCopy[item] );
      }
      this.searchResults = true;
    }
  }

  makeEntriesList( results )
  {
    for ( var item in results )
    {
      results[item] = this.helperService.addMetaDataFlags( results[item] );
      results[item] = this.helperService.addNoTitle( results[item] );

      // Check follow-up entries
      if ( results[item].FollowUpSurveyAttempts.length > 0 )
      {
        for ( var fu in results[item].FollowUpSurveyAttempts )
        {
          results[item].FollowUpSurveyAttempts[fu] = this.helperService.addMetaDataFlags(results[item].FollowUpSurveyAttempts[fu]);
          results[item].FollowUpSurveyAttempts[fu] = this.helperService.addNoTitle(results[item].FollowUpSurveyAttempts[fu]);
        }
      }
      this.entries.push( results[item] );
    }
  }

  clearSearchLogEntries()
  {
    // Restore original entry list.
    for ( var item in this.entriesCopy )
    {
      this.entries.push( this.entriesCopy[item] );
    }
    this.searchResults = true;
  }

  showActivityPage( surveyAttempt )
  {
    this.events.publish('activity:view', surveyAttempt);
  }

  removeActivityFromList( surveyAttemptId )
  {
    // Remove list item
    for ( var i = 0; i <= this.entries.length - 1; i++ )
    {
      if ( this.entries[i].SurveyAttemptsId == surveyAttemptId )
      {
        this.entries.splice(i, 1);
      }

      // This may be a follow up entry.  Let's check.
      if ( typeof this.entries[i].FollowUpSurveyAttempts !== 'undefined' )
      {
        for ( var j = 0; j <= this.entries[i].FollowUpSurveyAttempts.length - 1; j++ )
        {
          if ( this.entries[i].FollowUpSurveyAttempts[j].SurveyAttemptsId == surveyAttemptId )
          {
            this.entries[i].FollowUpSurveyAttempts.splice(j, 1);
          }
        }
      }
    }
  }

  refreshActivityList( surveyAttemptId )
  {
    this.showIntroPanel = false;
    this.surveyService.getSurveyAttempt( surveyAttemptId ).subscribe(data => {
      // Loop over existing list and replace data
      var entry = data.json().data;
      entry = this.helperService.addMetaDataFlags( entry );

      var updatedExistingEntry = false;
      for ( var i = 0; i <= this.entries.length - 1; i ++ )
      {
        // First, is this a follow up entry?
        if ( entry.SurveyAttemptsFollowUpToId > 0 )
        {
          for ( var j = 0; j <= this.entries[i].FollowUpSurveyAttempts.length - 1; j ++ )
          {
            if ( this.entries[i].FollowUpSurveyAttempts[j].SurveyAttemptsId === surveyAttemptId )
            {
              this.entries[i].FollowUpSurveyAttempts[j] = entry;
              updatedExistingEntry = true;
            }
          }
        }
        if ( this.entries[i].SurveyAttemptsId === surveyAttemptId )
        {
          this.entries[i] = entry;
          updatedExistingEntry = true;
        }
      }

      if ( updatedExistingEntry === false )
      {
        // Then we've added a new log entry and need to add it to the list.
        // But first, is this a follow up entry?
        if ( entry.SurveyAttemptsFollowUpToId > 0 )
        {
          // Loop over entries, find the correct entry and add to its
          // FollowUpSurveyAttempts array
          for ( var i = 0; i <= this.entries.length - 1; i ++ )
          {
            if ( this.entries[i].SurveyAttemptsId === entry.SurveyAttemptsFollowUpToId )
            {
              this.entries[i].FollowUpSurveyAttempts.unshift( entry );
            }
          }
        } else {
          // Just pop this sucker at the top of the list.
          this.entries.unshift( entry );
        }
      }
    });
  }

  ngOnInit()
  {
    this.getLogEntries( null );
  }
}
