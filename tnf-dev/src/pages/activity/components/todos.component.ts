import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular';
import { Constants } from '../../../providers/config/config';
import { SurveyService } from '../../../providers/survey-service/survey-service';
import { ToDosService } from '../../../providers/todos-service/todos-service';
import { HelperService } from '../../../providers/helper-service/helper-service';

@Component({
  selector: 'todos',
  templateUrl: 'todos.component.html'
})
export class ToDosComponent {
  @Input() surveyAttempt: any;
  todos: any = [];
  surveyAttemptId: number;
  newToDo: string = '';

  constructor(
    private surveyService: SurveyService,
    private helperService: HelperService,
    private todosService: ToDosService,
    private events: Events
  ) {
  }

  saveToDo()
  {
    let surveyMetaDataEntryId = this.helperService.createRandomString(15);

    // ToDo content data
    let data = {
      SurveyMetaDataEntryId: surveyMetaDataEntryId,
      SurveyMetaDataSurveyId: this.surveyAttempt.SurveyAttemptsSurveyId,
      SurveyMetaDataSurveyAttemptId: this.surveyAttempt.SurveyAttemptsId,
      SurveyMetaDataEntryType: 'todos',
      SurveyMetaDataKey: 'todos',
      SurveyMetaDataValue: this.newToDo
    };

    // ToDo status
    let status = {
      SurveyMetaDataEntryId: surveyMetaDataEntryId,
      SurveyMetaDataSurveyId: this.surveyAttempt.SurveyAttemptsSurveyId,
      SurveyMetaDataSurveyAttemptId: this.surveyAttempt.SurveyAttemptsId,
      SurveyMetaDataEntryType: 'todos',
      SurveyMetaDataKey: 'todos_status',
      SurveyMetaDataValue: 'incomplete'
    };

    if ( this.newToDo.length > 0 )
    {
      this.surveyService.saveSurveyMetaData( data ).subscribe(data => {
        this.events.publish('activity:updateactivitydata', this.surveyAttempt.SurveyAttemptsId);
        this.events.publish('app:log', {
          message: 'To-do list has been saved.',
          status: 'success',
          type: 'save todo'
        });
      });

      this.surveyService.saveSurveyMetaData( status ).subscribe(data => {});

      let newToDoEntry = {
        type: 'todos',
        entryId: surveyMetaDataEntryId,
        date: 'Now',
        todos: this.newToDo,
        todos_status: 'incomplete'
      };

      this.todos.unshift( newToDoEntry );
      this.newToDo = '';
    }
  }

  deleteToDo( todo )
  {
    this.todosService.deleteToDo( todo.entryId ).subscribe(data => {
      // Remove this item from the list
      this.todos.splice( this.todos.indexOf(todo), 1 );
    });
  }

  toggleToDoComplete( todo )
  {
    this.todosService.toggleToDoComplete( todo.entryId, todo.todos_status ).subscribe(data => {
      if ( todo.todos_status === 'complete' )
      {
        todo.todos_status = 'incomplete';
      } else {
        todo.todos_status = 'complete';
      }
    });
  }

  refreshToDoList()
  {
    this.todos = this.helperService.getMetaDataItem( 'todos', this.surveyAttempt );
  }

  ngOnInit()
  {
  }
}
