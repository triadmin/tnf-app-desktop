import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { App, NavController, ActionSheetController, NavParams, ModalController, Alert, Events } from 'ionic-angular';
import { GoogleAnalytics } from 'ionic-native';

import { Constants } from '../../providers/config/config';

// Components
import { ToDosComponent } from '../../pages/activity/components/todos.component';

// Pages
import { SurveyPage } from '../../pages/survey/survey';

// Modals
import { NotificationsModalPage } from '../../pages/activity/components/notifications-modal';

// Services
import { SurveyService } from '../../providers/survey-service/survey-service';
import { UserService } from '../../providers/user-service/user-service';
import { HelperService } from '../../providers/helper-service/helper-service';
import { PrintService } from '../../providers/print-service/print-service';

// Pipes
import { NewlinePipe } from '../../app/pipes/common.pipes';

@Component({
  templateUrl: 'activity-view.html'
})

export class ActivityViewPage {
  @ViewChild('activityDom') activityDom: ElementRef;
  @ViewChild('todos') todos: ToDosComponent;

  activity: any;
  metaData: any;
  surveyAttempt: any;
  surveyQuestions: any;
  user: any = Constants.USER;

  // For template
  activityTypes: any = [];
  audienceTypes: any = [];
  deliverables: any = [];
  comments: string = '';
  taMode: string = '';
  duration: string = '';
  location: any = {
    Counties: [],
    Districts: [],
    LocationObject: []
  };

  followUpActivities: any = [];
  notifications: any = [];
  notification_to_from: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public surveyService: SurveyService,
    public helperService: HelperService,
    public userService: UserService,
    public events: Events,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    public printService: PrintService
  )
  {
    // If we're pushing the view screen onto the stack
    // for the first time...
    this.surveyAttempt = this.navParams.get('surveyAttempt');
    this.getFullActivity( this.surveyAttempt.SurveyAttemptsId );

    // If the view page is already on the stack, just update
    // the activity object.
    this.events.subscribe('activity:updateview', ( surveyAttempt ) => {
      this.surveyAttempt = surveyAttempt;
      this.getFullActivity( this.surveyAttempt.SurveyAttemptsId );
    });

    // After editing survey, we need to render updated data.
    this.events.subscribe('activity:updateactivitydata', ( survey_attempt_id ) => {
      this.getFullActivity( survey_attempt_id );
    });
  }

  // Get the full activity - all questions and responses.
  getFullActivity( surveyAttemptId )
  {
    Constants.SURVEY_ATTEMPT_ID = surveyAttemptId;
    this.surveyService.getSurvey().subscribe(data => {
      this.activity = data.json().data;
      this.surveyAttempt = this.activity.survey_attempt;
      this.surveyQuestions = this.activity.survey_questions;
      this.followUpActivities.length = 0;

      // See if we can get any follow-up activities
      this.surveyService.getSurveyFollowUpAttempts( surveyAttemptId ).subscribe(data => {
        this.followUpActivities = data.json().data;
        this.populateTemplate();
      });
    });
  }

  populateTemplate()
  {
    this.audienceTypes.length = 0;
    this.activityTypes.length = 0;
    this.deliverables.length = 0;
    this.notifications.length = 0;

    this.comments = 'No Comments';
    this.taMode = 'Not reported';
    this.duration = 'Not reported';

    this.location = {
      Counties: [],
      Districts: [],
      LocationObject: []
    };

    this.getQuestionResponse( 'TypeOfActivity' );
    this.getQuestionResponse( 'Audience' );
    this.getQuestionResponse( 'Deliverables' );
    this.getQuestionResponse( 'Comments' );
    this.getQuestionResponse( 'TechnicalAssistanceMode' );
    this.getQuestionResponse( 'AreaInvolved' );
    this.getQuestionResponse( 'DurationOfActivity' );

    this.surveyAttempt = this.helperService.addNoTitle( this.surveyAttempt );
    this.notifications = this.helperService.getMetaDataItem( 'notification', this.surveyAttempt );

    // Refresh ToDos list
    this.todos.refreshToDoList();
  }

  getQuestionResponse( qIdentifier )
  {
    for ( let page in this.surveyQuestions )
    {
      let p = this.surveyQuestions[page];
      for ( let question in p )
      {
        let q = p[question];
        if ( q.SurveyQuestionsIdentifier === qIdentifier )
        {
          switch ( qIdentifier )
          {
            case 'TypeOfActivity':
              for ( let response in q.Responses )
              {
                this.activityTypes.push( q.Responses[response].SurveyResponsesText );
              }
              break;
            case 'Audience':
              for ( let response in q.Responses )
              {
                this.audienceTypes.push(
                  '<strong>[' + q.Responses[response].SurveyResponsesValue + ']</strong>&nbsp;' +
                  q.Responses[response].SurveyResponsesText
                );
              }
              break;
            case 'Deliverables':
              for ( let response in q.Responses )
              {
                this.deliverables.push( q.Responses[response].SurveyResponsesText );
              }
              break;
            case 'Comments':
              if ( typeof q.Responses[0] !== 'undefined' )
              {
                this.comments = q.Responses[0].SurveyResponsesText;
              }
              break;
            case 'TechnicalAssistanceMode':
              if ( typeof q.Responses[0] !== 'undefined' )
              {
                this.taMode = q.Responses[0].SurveyResponsesText;
              }
              break;
            case 'DurationOfActivity':
              if ( typeof q.Responses[0] !== 'undefined' )
              {
                this.duration = q.Responses[0].SurveyResponsesText;
              }
              break;
            case 'AreaInvolved':
              // Parse SurveyResponseText JSON
              try {
                 var respObj = JSON.parse( q.Responses[0].SurveyResponsesText );
                 if ( respObj.counties && respObj.counties.length > 0 )
                 {
                   this.location.Counties = respObj.counties;
                 }
                 if ( respObj.districts && respObj.districts.length > 0 )
                 {
                   this.location.Districts = respObj.districts;
                 }
                 this.location.LocationObject = respObj;
              } catch (e) {
              }
              break;
          }
        }
      }
    }
  }

  viewFollowUpEntry( fuActivity )
  {
    this.events.publish('activity:view', fuActivity);
  }

  newFollowUpEntry()
  {
    this.navCtrl.push(SurveyPage, {
      followUpToSurveyAttemptsId: this.surveyAttempt.SurveyAttemptsId
    });
  }

  editActivity()
  {
    var surveyAttemptsId = this.surveyAttempt.SurveyAttemptsId;
    this.navCtrl.push(SurveyPage, {
      surveyAttemptsId: surveyAttemptsId
    });
  }

  moreOptionsShowActionSheet()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'More options for this activity.',
      buttons: [
        {
          text: 'Send a Notification',
          handler: () => {
            this.presentNotificationsModal();
          }
        },
        {
          text: 'Print',
          handler: () => {
            this.printActivity();
          }
        },
        {
          text: 'Delete This Activity',
          role: 'destructive',
          handler: () => {
            this.deleteEntryShowActionSheet();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    actionSheet.present();
  }

  deleteEntryShowActionSheet()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Cancel and Delete This Activity Entry?',
      buttons: [
        {
          text: 'Delete This Activity',
          role: 'destructive',
          handler: () => {
            this.deleteActivity();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    actionSheet.present();
  }

  printActivity()
  {
    let html = this.activityDom.nativeElement.innerHTML;
    this.printService.printPage( html );
  }

  deleteActivity()
  {
    this.navCtrl.pop().then(() => {});

    this.surveyService.deleteSurveyAttempt( this.surveyAttempt.SurveyAttemptsId ).subscribe(data => {
      // Remove activity from activity list.
      this.events.publish('activity:removefromlist', this.surveyAttempt.SurveyAttemptsId);
    });
  }

  presentNotificationsModal()
  {
    let notificationsModal = this.modalCtrl.create( NotificationsModalPage, {
      surveyAttempt: this.surveyAttempt,
      metaData: this.surveyAttempt.MetaData
    });
    notificationsModal.present();
  }

  ngAfterViewInit()
  {

  }
}
