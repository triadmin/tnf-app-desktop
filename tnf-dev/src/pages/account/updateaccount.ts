import { Component } from '@angular/core';
import { Events, NavController, MenuController, AlertController } from 'ionic-angular';
import { GoogleAnalytics } from 'ionic-native';

import { LoginService } from '../../providers/login-service/login-service';
import { AccountService } from '../../providers/account-service/account-service';
import { Constants } from '../../providers/config/config';
import { LoginPage } from '../login/login';

@Component({
  templateUrl: 'updateaccount.html'
})

export class UpdateAccountPage {
  accountForm: any = {};
  passwordForm: any = {};
  user: any = Constants.USER;
  formError: any = {
    error: false,
    errorType: '',
    errorMessage: ''
  };
  formSuccess: any = {
    success: true,
    successType: '',
    successMessage: ''
  };

  constructor(
    private nav: NavController,
    private menuCtrl: MenuController,
    private events: Events,
    private accountService: AccountService,
    private loginService: LoginService
  ) {
    this.initForm();
    GoogleAnalytics.trackView('Update Account');
  }

  initForm()
  {
    this.accountForm.UsersFirstName = this.user.UsersFirstName;
    this.accountForm.UsersLastName = this.user.UsersLastName;
    this.accountForm.UsersEmail = this.user.UsersEmail;
    this.accountForm.UsersRegion = this.user.UsersRegion;
  }

  updateAccount()
  {
    // Error checking
    if ( this.accountForm.UsersFirstName.length === 0 || this.accountForm.UsersLastName.length === 0 || this.accountForm.UsersEmail.length === 0 )
    {
      this.formSuccess = {
        success: false,
        successType: 'account',
        successMessage: 'Your account was successfully updated.'
      };
      this.formError = {
        error: true,
        errorType: 'account',
        errorMessage: 'You are missing some information.'
      };
      return false;
    }

    // Send to API
    let user = {
      UsersFirstName: this.accountForm.UsersFirstName,
      UsersLastName: this.accountForm.UsersLastName,
      UsersEmail: this.accountForm.UsersEmail,
      UsersRegion: this.accountForm.UsersRegion,
      UsersId: this.user.UsersId
    };
    this.accountService.updateAccount( user ).subscribe(data => {
      // Update Constants.USER object
      this.accountService.getUserRecord().subscribe(data => {
        Constants.USER = data.json().data;
      });

      this.formError = {
        error: false,
        errorType: 'account',
        errorMessage: ''
      };
      this.formSuccess = {
        success: true,
        successType: 'account',
        successMessage: 'Your account was successfully updated.'
      };

      GoogleAnalytics.trackEvent('Account', 'update-account', 'Update Account Information');
    });
  }

  updatePassword()
  {
    // Error checking
    if ( this.passwordForm.password !== this.passwordForm.passwordConfirm )
    {
      this.formError = {
        error: true,
        errorType: 'password',
        errorMessage: 'You did not confirm your new password.'
      };
      return false;
    }

    // Error checking
    if ( typeof this.passwordForm.password === 'undefined' )
    {
      this.formError = {
        error: true,
        errorType: 'password',
        errorMessage: 'You did not enter a new password.'
      };
      return false;
    }

    // Send to API
    this.accountService.resetPassword( this.passwordForm.password, this.passwordForm.passwordConfirm, this.user.UsersId ).subscribe(data => {
      this.formError = {
        error: false,
        errorType: 'password',
        errorMessage: ''
      };
      this.formSuccess = {
        success: true,
        successType: 'password',
        successMessage: 'Your password was successfully changed.'
      };

      GoogleAnalytics.trackEvent('Account', 'change-password', 'Change Password');
    });
  }

  accountLogout()
  {
    this.nav.setRoot(LoginPage);
    this.loginService.logout().then((data) => {
      this.events.publish('user:logout');
    });

    GoogleAnalytics.trackEvent('Account', 'logout', 'Log Out');
  }
}
