import { Component, OnInit } from '@angular/core';
import { App, NavController, Events, AlertController, NavParams, ActionSheetController } from 'ionic-angular';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { GoogleAnalytics } from 'ionic-native';

import { SurveyService } from '../../providers/survey-service/survey-service';
import { Constants } from '../../providers/config/config';
import { Keyboard } from 'ionic-native';

// Import question types
import {SurveyAttemptComponent} from './question_types/survey-attempt';
import {HeadingComponent} from './question_types/heading';
import {RadioComponent} from './question_types/radio';
import {TextComponent} from './question_types/text';
import {TextareafieldComponent} from './question_types/textarea';
import {CheckboxComponent} from './question_types/checkbox';
import {DatetimeComponent} from './question_types/datetime';
import {DatepickerComponent} from './question_types/datepicker';
import {SelectComponent} from './question_types/select';
import {SelectmultiComponent} from './question_types/selectmulti';
import {ListWithHeadersCheckboxComponent} from './question_types/list-with-headers-checkbox';
import {ListWithHeadersTextComponent} from './question_types/list-with-headers-text';
import {SelectCountyDistrictSchoolComponent} from './question_types/select-county-district-school';

@Component({
  templateUrl: 'survey.html',
})
export class SurveyPage {
  user: any;
  survey_questions: any;
  page_questions: any;
  survey_meta: any;
  survey_page: number = 1;
  last_survey_page: number;
  survey_attempt_id: any = 0;
  survey_attempt_data: any = {};
  survey_edit_mode: boolean = false;
  mode_text: string = 'Add New';

  constructor(
    private nav: NavController,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private actionSheetCtrl: ActionSheetController,
    private surveyService: SurveyService,
    private navParams: NavParams,
    private events: Events
  ) {
    this.formBuilder = formBuilder;
    this.surveyService = surveyService;
    this.user = Constants.USER;
    this.navParams = navParams;

    Constants.SURVEY_ATTEMPT_ID = 0;
    if ( this.navParams.get('surveyAttemptsId') )
    {
      Constants.SURVEY_ATTEMPT_ID = this.navParams.get('surveyAttemptsId');
    }

    GoogleAnalytics.trackView('Activity Entry Form');
  }

  getSurvey()
  {
    this.surveyService.getSurvey().subscribe(data => {
      this.survey_questions = data.json().data.survey_questions;
      this.survey_meta = data.json().data.survey_title;
      this.survey_attempt_data = data.json().data.survey_attempt;

      // Figure out last survey page.
      var last_el = Object.keys(this.survey_questions)[Object.keys(this.survey_questions).length - 1];
      var last_el_array = last_el.split('_');
      this.last_survey_page = parseInt(last_el_array[1]);

      this.renderSurveyPage();

      if ( Constants.SURVEY_ATTEMPT_ID === 0 )
      {
        // Are we creating a new survey attempt?
        this.createSurveyAttempt();
        this.survey_edit_mode = false;
      } else {
        // Or are we editing an old survey?
        this.survey_attempt_id = Constants.SURVEY_ATTEMPT_ID;
        this.survey_edit_mode = true;
        this.mode_text = 'Edit Your';
      }
    });
  }

  renderSurveyPage()
  {
    // Render questions for current survey page.
    this.page_questions = this.survey_questions['Page_' + this.survey_page];
  }

  createSurveyAttempt()
  {
    this.surveyService.createSurveyAttempt().subscribe(data => {
      this.survey_attempt_id = data.json().data.survey_attempt_id;

      if ( this.navParams.get('followUpToSurveyAttemptsId') )
      {
        let followUpToSurveyAttemptsId = this.navParams.get('followUpToSurveyAttemptsId');
        let answerObj = {
          survey_attempt_id: this.survey_attempt_id,
          question_type: 'followUpSurveyId',
          answer: followUpToSurveyAttemptsId
        };
        this.surveyService.saveSurveyAttemptResponse( answerObj ).subscribe(data => {});
        this.survey_attempt_data.SurveyAttemptsFollowUpToId = followUpToSurveyAttemptsId;
      }

      // Save survey_attempt_id in Constants.
      Constants.SURVEY_ATTEMPT_ID = this.survey_attempt_id;
    });
  }

  cancel()
  {
    let confirm = this.actionSheetCtrl.create({
      title: 'Cancel and Delete This Activity Entry?',
      buttons: [
        {
          text: 'Delete This Activity',
          role: 'destructive',
          handler: () => {
            this.cancelSurveyAttempt();
          }
        },
        {
          text: 'No, I Want To Keep Going',
          role: 'cancel',
          handler: () => {
            this.continueSurveyAttempt();
          }
        }
      ]
    });
    confirm.present();
  }

  save()
  {
    // Send user back to activity view page.
    this.nav.pop();

    // Fire off event so we can update activity view page with edited data.
    this.events.publish( 'activity:updateactivitydata', this.survey_attempt_id );

    // Fire off log event.
    this.events.publish('app:log', {
      message: 'Activity data has been saved.',
      type: 'save activity',
      status: 'success'
    });
  }

  cancelSurveyAttempt()
  {
    // Delete this current survey attempt and
    // take user back to dashboard page.

    this.nav.pop().then(() => {});
    this.surveyService.deleteSurveyAttempt( this.survey_attempt_id ).subscribe(data => { });
  }

  continueSurveyAttempt()
  {

  }

  switchPage( direction )
  {
    if ( direction == 'next' )
    {
      this.survey_page++;
    } else {
      if ( this.survey_page > 1 )
      {
        this.survey_page--;
      }
    }

    this.renderSurveyPage();
    Keyboard.close();
  }

  ngOnInit()
  {
    this.getSurvey();
  }
}
