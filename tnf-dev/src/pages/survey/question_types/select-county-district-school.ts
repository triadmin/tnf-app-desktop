import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';
import { PlacesService } from '../../../providers/places-service/places-service';
import { Constants } from '../../../providers/config/config';
import { Observable } from 'rxjs/Observable';

declare var google;

@Component({
  selector: 'select-county-district-school',
  template: `
    <ion-list>
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
      <ion-item>
        <ion-label>Entire Region</ion-label>
        <ion-checkbox green [checked]="response.region == true" (click)="saveResponse($event, question, 'region')" value="Entire Region" ></ion-checkbox>
      </ion-item>
      <ion-item>
        <ion-label>County</ion-label>
        <ion-select [disabled]="response.region == true" multiple="true" [(ngModel)]="response.counties" (ionChange)="saveResponse($event, counties, 'county')">
          <ion-option *ngFor="let county of counties" value="{{ county.county }}">{{ county.county }}</ion-option>
        </ion-select>
      </ion-item>
      <ion-item>
        <ion-label>School District</ion-label>
        <ion-select [disabled]="response.region == true" multiple="true" [(ngModel)]="response.districts" (ionChange)="saveResponse($event, question, 'district')">
          <ion-option *ngFor="let district of districts" value="{{ district.districtName }}">{{ district.districtName }}</ion-option>
        </ion-select>
      </ion-item>
      <ion-item>
        <ion-input type="text" clearInput
          placeholder="School, Program or Agency"
          [(ngModel)]="response.school"
          [disabled]="response.region == true"
          (blur)="saveResponse($event, question, 'school')"
          (keyup)="filterPlaces($event)"
        ></ion-input>
      </ion-item>
      <div *ngIf="places.length > 0">
        <ion-list-header *ngIf="placesRenderList.length > 0" style="border-bottom: none;">Select from existing school or program</ion-list-header>
        <ion-item class="pill-container">
          <a class="pill" *ngFor="let place of placesRenderList" (click)="selectPlace(place)">{{ place }}</a>
        </ion-item>
      </div>
    </ion-list>
  `
})

export class SelectCountyDistrictSchoolComponent
{
  @Input() question: any;
  response: any;
  counties: any;
  selectedCounties: any;
  districts: any[] = [];
  selectedDistricts: any;
  schools: any;
  region: boolean = false;
  answerJSON: any;
  user: any = Constants.USER;
  formattedAnswer: any;
  qId: number;
  places: any = Constants.PLACES;
  placesRenderList: any = [];

  coordsCounty: any;
  coordsDistrict: any;
  coordsSchool: any;

  constructor(
    private surveyService: SurveyService,
    private placesService: PlacesService
  ) {
    this.surveyService = surveyService;
    this.placesService = placesService;
  }

  saveResponse(answer, question, level)
  {
    this.response.regionText = 'No';
    this.response.school = '';
    var place = [];
    if ( level == 'county' )
    {
      this.selectedCounties = answer;
      this.populateDistrictsFromCounties( answer );
      place = this.selectedCounties;
    } else if ( level == 'district' ) {
      this.selectedDistricts = answer;
      place = this.selectedDistricts;
    } else if ( level == 'region' ) {
      this.response.region = !this.response.region;
      if ( this.response.region == true )
      {
        this.response.regionText = 'Yes';
      }
      this.disableFormControls();
    } else if ( level == 'school' ) {
      this.response.region = false;
      this.response.regionText = 'No';
      this.response.school = answer.target.value;
      place.push( this.response.school );
    }

    var responseObj = {
      'counties': this.selectedCounties,
      'districts': this.selectedDistricts,
      'region': this.response.regionText,
      'school': this.response.school,
      'coords': {
        'county': [],
        'district': [],
        'school': []
      }
    }

    // Let's save to server before trying to get
    // coords from Google Places.  If the API call fails,
    // then we don't save our location and this sucks!
    var responseString = JSON.stringify( responseObj );
    var answerObj = {
      'SurveyAnswersSurveyQuestionsId': this.qId,
      'SurveyAnswersText': responseString
    }
    this.saveToServer( answerObj );

    if ( place.length > 0 )
    {
      var placesCount = place.length;
      var a = 1;
      var coordArray = [];
      this.getPlaceLatLong( place ).subscribe(data => {
        coordArray.push( data );
        if ( a === place.length )
        {
          // We've got all of our coodinates. Save!
          if ( level === 'county' ) this.coordsCounty = coordArray;
          if ( level === 'district' ) this.coordsDistrict = coordArray;
          if ( level === 'school' ) this.coordsSchool = coordArray;

          responseObj.coords.county = this.coordsCounty;
          responseObj.coords.district = this.coordsDistrict;
          responseObj.coords.school = this.coordsSchool;
          var responseString = JSON.stringify( responseObj );
          var answerObj = {
            'SurveyAnswersSurveyQuestionsId': this.qId,
            'SurveyAnswersText': responseString
          }
          this.saveToServer( answerObj );
        }
        a++;
      });
    } else {
      var responseString = JSON.stringify( responseObj );
      var answerObj = {
        'SurveyAnswersSurveyQuestionsId': this.qId,
        'SurveyAnswersText': responseString
      }
      this.saveToServer( answerObj );
      //this.surveyService.saveResponse( answerObj, 'select-county-district-school' ).subscribe(data => {});
    }
  }

  saveToServer( answerObj )
  {
    this.surveyService.saveResponse( answerObj, 'select-county-district-school' ).subscribe(data => {});
  }

  getPlaceLatLong( place )
  {
    return Observable.create((observer) => {
      var request;
      let service = new google.maps.places.PlacesService(document.createElement('div'));

      for ( var a = 0; a <= place.length - 1; a ++ )
      {
        request = {
          query: place[a] + ' Oregon'
        }

        service.textSearch(request, ( results, status ) => {
          if (status == google.maps.places.PlacesServiceStatus.OK) {
            var lat = results[0].geometry.location.lat();
            var lng = results[0].geometry.location.lng();
            var obj = {
              'lat': lat,
              'lng': lng
            };

            observer.next(obj);
          }
        });
      }
    });
  }

  filterPlaces( ev )
  {
    let val = ev.target.value;

    if ( val.length > 3 )
    {
      this.placesRenderList = this.places.filter((item) => {
        return (
          item.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
          item.toLowerCase().indexOf(val.toLowerCase()) > -1
        );
      });
    }
  }

  selectPlace( place )
  {
    this.response.school = place;
    var answer = {
      'target': {
        'value': place
      }
    }
    this.saveResponse(answer, this.question, 'school')
  }

  populateDistrictsFromCounties( countyObj )
  {
    this.districts = [];
    // TODO: document this sucker!
    // Loop over each county and push districts for that county to the districts object.
    if ( typeof countyObj !== 'undefined' )
    {
      for ( var a = 0; a <= countyObj.length - 1; a++ )
      {
        for ( var i = 0; i <= this.answerJSON[0][this.user.UsersRegion]['counties'].length-1; i++ )
        {
          var county_obj = this.answerJSON[0][this.user.UsersRegion]['counties'][i];
          if ( county_obj.county == countyObj[a] )
          {
            for ( var c = 0; c <= county_obj.districts.length - 1; c++ )
            {
              this.districts.push( county_obj.districts[c] );
            }
          }
        }
      }
    }
  }

  disableFormControls()
  {
    // If we select Entire Region, the county, district, and school fields should
    // be cleared and disabled.

  }

  renderResponse()
  {
    // Look for any reponses.
    if ( this.question.Responses.length > 0 )
    {
      var responseObj = this.question.Responses;
      this.response = eval('(' + this.question.Responses[0].SurveyResponsesText + ')');

      // Populate district select with county options
      this.populateDistrictsFromCounties( this.response.counties );

      // Convert region.
      if ( this.response.region == 'Yes' )
      {
        this.response.region = true;
      }
    } else {
      this.response = {
        county: '',
        district: '',
        school: '',
        region: '',
        regionText: ''
      };
    }
  }

  ngOnInit()
  {
    // Do we have a question with SurveyAnswersJSON?
    if ( typeof this.question.answers[0].SurveyAnswersJSON !== 'undefined' )
    {
      // NOTE: This does not work universally. It only works for Region, counties,
      //       and school districts right now.
      this.answerJSON = eval("(" + this.question.answers[0].SurveyAnswersJSON + ")");
      this.counties = this.answerJSON[0][this.user.UsersRegion]['counties'];
    }
    this.renderResponse();

    // Set questionId
    this.qId = this.question.SurveyQuestionsId;
  }
}
