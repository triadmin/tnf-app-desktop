import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';

@Component({
  selector: 'text',
  template: `
    <ion-list>
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
      <ion-item>
        <ion-input autocorrect="on" type="text" (blur)="saveResponse($event)" [(ngModel)]="response" clearInput></ion-input>
      </ion-item>
    </ion-list>
  `
})

export class TextComponent
{
  @Input() question: any;
  response: any;

  constructor( private surveyService: SurveyService ) {
    this.surveyService = surveyService;
  }

  saveResponse( event )
  {
    var answer = {
      'SurveyAnswersValue': '',
      'SurveyAnswersText': event.target.value,
      'SurveyAnswersSurveyQuestionsId': this.question.SurveyQuestionsId,
      'SurveyAnswersId': 0
    };
    this.surveyService.saveResponse( answer, 'text' ).subscribe(data => {});
  }

  renderResponse()
  {
    if ( this.question.Responses.length > 0 )
    {
      var responseObj = this.question.Responses;
      this.response = responseObj[0].SurveyResponsesText;
    }
  }

  ngOnInit()
  {
    this.renderResponse();
  }
}
