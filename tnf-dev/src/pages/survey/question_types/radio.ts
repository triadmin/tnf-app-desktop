import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';

@Component({
  selector: 'radio',
  template: `
    <ion-list [(ngModel)]="response" radio-group>
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
      <ion-item *ngFor="let answer of question.answers">
        <ion-label>{{ answer.SurveyAnswersText }}</ion-label>
        <ion-radio type="radio" (click)="saveResponse(answer)" value="{{ answer.SurveyAnswersText }}"></ion-radio>
      </ion-item>
    </ion-list>
  `
})

export class RadioComponent
{
  @Input() question: any;
  response: any;

  constructor( private surveyService: SurveyService ) {
    this.surveyService = surveyService;
  }

  saveResponse( answer )
  {
    this.surveyService.saveResponse( answer, 'radio' ).subscribe(data => {});
  }

  renderResponse()
  {
    // Look for any reponses (this is a radio button, we're only going to have 1 response.)
    if ( this.question.Responses.length > 0 )
    {
      var responseObj = this.question.Responses;
      this.response = responseObj[0].SurveyResponsesText;
    }
  }

  ngOnInit()
  {
    this.renderResponse();
  }
}
