import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';

@Component({
  selector: 'select-list',
  template: `
    <ion-list>
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
      <ion-item>
        <ion-label>{{ question.SurveyQuestionsText }}</ion-label>
        <ion-select [(ngModel)]="response" (ionChange)="saveResponse($event, question)">
          <ion-option *ngFor="let answer of question.answers" value="{{ answer.SurveyAnswersText }}">{{ answer.SurveyAnswersText }}</ion-option>
        </ion-select>
      </ion-item>
    </ion-list>
  `
})

export class SelectComponent
{
  @Input() question: any;
  response: any;

  constructor( private surveyService: SurveyService ) {
    this.surveyService = surveyService;
  }

  saveResponse(answer, question)
  {
    for ( var i = 0; i <= question.answers.length - 1; i++ )
    {
      if ( question.answers[i].SurveyAnswersText == answer )
      {
        this.surveyService.saveResponse( question.answers[i], 'select' ).subscribe(data => {});
      }
    }
  }

  renderResponse()
  {
    // Look for any reponses.
    if ( this.question.Responses.length > 0 )
    {
      var responseObj = this.question.Responses;
      this.response = responseObj[0].SurveyResponsesText;
    }

    var answers = this.question.answers;
    for ( var i = 0; i <= answers.length - 1; i++ )
    {
      if ( typeof answers[i]['Response'] != 'undefined' )
      {
        var responseObj = answers[i]['Response'][0];

        this.response = responseObj.SurveyResponsesText;
      }
    }
  }

  ngOnInit()
  {
    this.renderResponse();
  }
}
