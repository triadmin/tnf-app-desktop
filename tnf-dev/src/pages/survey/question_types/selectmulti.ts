import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';

@Component({
  selector: 'select-list-multi',
  template: `
    <ion-list>
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
      <ion-item>
        <ion-label>{{ question.SurveyQuestionsText }}</ion-label>
        <ion-select  multiple="true" [(ngModel)]="response" (ionChange)="saveResponse($event, question)">
          <ion-option *ngFor="let answer of question.answers" value="{{ answer.SurveyAnswersText }}">{{ answer.SurveyAnswersText }}</ion-option>
        </ion-select>
      </ion-item>
    </ion-list>
  `
})

export class SelectmultiComponent
{
  @Input() question: any;
  response: any = [];
  qId: number;

  constructor( private surveyService: SurveyService ) {
    this.surveyService = surveyService;
  }

  saveResponse(answer, question)
  {
    for ( var i = 0; i <= answer.length - 1; i++ )
    {
      for ( var j = 0; j <= question.answers.length - 1; j++ )
      {
        if ( question.answers[j].SurveyAnswersText == answer[i] )
        {
          this.surveyService.saveResponse( question.answers[j], 'select-multi' ).subscribe(data => {});
        }
      }
    }
  }

  renderResponse()
  {
    // Look for any reponses.
    if ( this.question.Responses.length > 0 )
    {
      // Loop over answers.  Is an answer has a response, mark it 'checked'.
      var responseObj = this.question.Responses;
      var answersObj = this.question.answers;

      for ( var i = 0; i <= answersObj.length - 1; i++ )
      {
        for ( var j = 0; j <= responseObj.length - 1; j++ )
        {
          // Do we have a response for this answer?
          if ( responseObj[j].SurveyResponsesAnswersId === answersObj[i].SurveyAnswersId )
          {
            this.response.push( responseObj[j].SurveyResponsesText );
          }
        }
      }
    }
  }

  ngOnInit()
  {
    this.renderResponse();

    // Set questionId
    this.qId = this.question.SurveyQuestionsId;
  }
}
