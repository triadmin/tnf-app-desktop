import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';
import { DatePicker } from 'ionic-native';

@Component({
  selector: 'survey-attempt',
  template: `
    <ion-list>
      <ion-item>
        <ion-input [(ngModel)]="response.SurveyAttemptsTitle" type="text" (blur)="saveResponse($event, 'surveyTitle')" clearInput placeholder="Log Entry Title"></ion-input>
      </ion-item>
      <ion-item>
        <ion-label>Entry Date</ion-label>
        <ion-label item-right style="text-align: right;" (click)="openDatepicker()">{{ response.SurveyDate }}</ion-label>
      </ion-item>
      <ion-item>
        <ion-label>Follow Up To Activity:</ion-label>
        <ion-select [(ngModel)]="response.SurveyAttemptsFollowUpToId" (ionChange)="saveResponse($event, 'followUpSurveyId')">
          <ion-option *ngFor="let survey of entries" value="{{ survey.SurveyAttemptsId }}">
            <span *ngIf="survey.SurveyAttemptsTitle == null">[No Title]</span>
            <span *ngIf="survey.SurveyAttemptsTitle != null">{{ survey.SurveyAttemptsTitle }}</span>
          </ion-option>
        </ion-select>
      </ion-item>
      <ion-item>
        <ion-label>This Entry Requires Follow-up</ion-label>
        <ion-toggle [(ngModel)]="response.RequiresFollowUp" (ionChange)="saveResponse($event, 'requiresFollowUp')" checked="false"></ion-toggle>
      </ion-item>
    </ion-list>
  `
})

export class SurveyAttemptComponent
{
  @Input() public survey_attempt_id: any;
  @Input() public survey_attempt_data: any;

  // Create our response object
  // for when there's no data
  response: any = {
    SurveyDate: '',
    SurveyAttemptsTitle: null
  };
  entries: any;
  datePickerDate: any;

  constructor(
    private surveyService: SurveyService
  ) {
    this.surveyService = surveyService;
  }

  openDatepicker()
  {
    DatePicker.show({
      date: this.datePickerDate,
      mode: 'date'
    }).then(
      date => this.saveResponse(date, 'surveyDate')
    );
  }

  saveResponse(answer, question)
  {
    if ( question == 'surveyTitle' )
    {
      answer = answer.target.value;
    }
    if ( question == 'requiresFollowUp' )
    {
      if ( answer.checked === true )
      {
        answer = 'Yes';
      } else {
        answer = 'No';
      }
    }
    if ( question == 'surveyDate' && typeof answer !== 'undefined' )
    {
      var datepickerValue = answer;
      var day = answer.getDate();
      var month = (answer.getMonth() + 1);
      var year = answer.getFullYear();

      if ( day < 10 ) { day = '0' + day; }
      if ( month < 10 ) { month = '0' + month; }
      answer = year + '-' + month + '-' + day;

      var date = new Date( datepickerValue );
      this.formatDate(date);
    }
    var answerObj = {
      answer: answer,
      question_type: question,
      survey_attempt_id: this.survey_attempt_id
    };
    this.surveyService.saveSurveyAttemptResponse( answerObj ).subscribe(data => {});
  }

  renderResponse()
  {
    this.response = this.survey_attempt_data;

    // Set date
    var date = new Date();
    this.formatDate(date);

    if ( typeof this.response.SurveyAttemptsSurveyDate !== "undefined" )
    {
      var dateParts = this.response.SurveyAttemptsSurveyDate.split('-');
      date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
      this.formatDate(date);
    }

    // Check to see if the Requires Follow Up toggle should be checked
    if ( this.response.SurveyAttemptsFollowUpRequired == 'Yes' )
    {
      this.response.RequiresFollowUp = true;
    }
  }

  formatDate( date )
  {
    var monthArray = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var dateString = monthArray[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
    this.response.SurveyDate = dateString;
    this.datePickerDate = date;
  }

  ngOnInit()
  {
    // Get any prior survey attempts.
    // This is so we can display attempts to select for a follow-up.
    // TODO: This isnt' scaleable.  What happens when we have 1000 surveys?
    // For now let's limit it to 40.
    this.surveyService.getSurveyAttempts('No', 0, 40).subscribe(data => {
      let result = data.json().data;
      this.entries = result;
    });

    this.renderResponse();
  }
}
