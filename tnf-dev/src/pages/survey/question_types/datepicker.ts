import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';
import { DatePicker } from 'ionic-native';

@Component({
  selector: 'datepicker',
  template: `
    <ion-list>
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
      <ion-item>
        <ion-label>Date</ion-label>
        <ion-input type="text" (click)="openDatepicker()" (ionChange)="saveResponse($event, 'date')" [(ngModel)]="date"></ion-input>
      </ion-item>
    </ion-list>
  `
})

export class DatepickerComponent
{
  @Input() question: any;
  @Input() include_time: boolean;
  response: any;
  date: any = new Date().toISOString();
  time: any = new Date().toISOString();

  constructor( private surveyService: SurveyService ) {
    this.surveyService = surveyService;
  }

  openDatepicker()
  {
    DatePicker.show({
      date: new Date(),
      mode: 'date'
    }).then(
      date => console.log('Got date: ', date),
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  saveResponse( event, type )
  {

  }

  renderResponse()
  {

  }

  ngOnInit()
  {
    this.renderResponse();
  }
}
