import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';

@Component({
  selector: 'datetime',
  template: `
    <ion-list>
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
      <ion-item>
        <ion-label>Date</ion-label>
        <ion-datetime displayFormat="MMM DD, YYYY" (ionChange)="saveResponse($event, 'date')" [(ngModel)]="date"></ion-datetime>
      </ion-item>
      <ion-item *ngIf="include_time != false">
        <ion-label>Time</ion-label>
        <ion-datetime disabled (ionChange)="saveResponse($event, 'time')" displayFormat="h:mm A" pickerFormat="h mm A" [(ngModel)]="time"></ion-datetime>
      </ion-item>
    </ion-list>
  `
})

export class DatetimeComponent
{
  @Input() question: any;
  @Input() include_time: boolean;
  response: any;
  date: any = new Date().toISOString();
  time: any = new Date().toISOString();

  constructor( private surveyService: SurveyService ) {
    this.surveyService = surveyService;
  }

  saveResponse( event, type )
  {
    var answer;
    if ( type == 'date' )
    {
      // Let's convert to timestamp.  That way it can be stored in a
      // MySQL text field and allows for easier conversion in JS.
      var month = event.month.value;
      var day = event.day.value;
      var year = event.year.value;
      var date = month + '/' + day + '/' + year;
      var timestamp = new Date( date ).getTime();
      answer = {
        'SurveyAnswersValue': 'date',
        'SurveyAnswersText': timestamp,
        'SurveyAnswersSurveyQuestionsId': this.question.SurveyQuestionsId,
        'SurveyAnswersId': 0
      };
      this.surveyService.saveResponse( answer, 'text' ).subscribe(data => {});
    } else if ( type == 'time' ) {
      /*

      We've disabeld time for the demo because it's overwritting the date.

      var hour = event.hour.value;
      var minute = event.minute.value;
      var ampm = event.ampm.text; // gives PM instead of pm. Not sure that's at all important.
      var time = hour + ':' + minute + ' ' + ampm;
      answer = {
        'SurveyAnswersValue': 'time',
        'SurveyAnswersText': time,
        'SurveyAnswersSurveyQuestionsId': this.question.SurveyQuestionsId,
        'SurveyAnswersId': 0
      };
      */
    }

    //this.surveyService.saveResponse( answer, 'text' ).subscribe(data => {});
  }

  renderResponse()
  {
    if ( this.question.Responses.length > 0 )
    {
      var responseObj = this.question.Responses;
      this.response = responseObj[0].SurveyResponsesText;

      // Convert timestamp to date
      this.date = new Date( parseInt( this.response, 10 ) ).toISOString();

      // Convert timestamp to time
    }
  }

  ngOnInit()
  {
    this.renderResponse();
  }
}
