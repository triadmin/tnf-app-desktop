import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';

@Component({
  selector: 'list-with-headers-checkbox',
  template: `
    <ion-list>
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
      <div *ngFor="let answer of question.answers; let i = index">
        <ion-list-header *ngIf="answer.SurveyAnswersExtra == 'header'">
          {{ answer.SurveyAnswersText }}
        </ion-list-header>

        <ion-item *ngIf="answer.SurveyAnswersExtra != 'header'">
          <ion-label>{{ answer.SurveyAnswersText }}</ion-label>
          <ion-checkbox green [(ngModel)]="answer.ischecked" (ionChange)="saveResponse(answer)"></ion-checkbox>
        </ion-item>
      </div>
    </ion-list>
  `
})

export class ListWithHeadersCheckboxComponent
{
  @Input() question: any;
  response: any;

  constructor( private surveyService: SurveyService ) {
    this.surveyService = surveyService;
  }

  saveResponse( answer )
  {
    answer.SurveyAnswersValue = '';
    this.surveyService.saveResponse( answer, 'list-with-headers-checkbox' ).subscribe(data => {
    });
  }

  renderResponse()
  {
    // Look for any reponses.
    if ( this.question.Responses.length > 0 )
    {
      // Loop over answers.  Is an answer has a response, mark it 'checked'.
      var responseObj = this.question.Responses;
      var answersObj = this.question.answers;

      for ( var i = 0; i <= answersObj.length - 1; i++ )
      {
        for ( var j = 0; j <= responseObj.length - 1; j++ )
        {
          // Do we have a response for this answer?
          if ( responseObj[j].SurveyResponsesAnswersId === answersObj[i].SurveyAnswersId )
          {
              answersObj[i].ischecked = true;
          }
        }
      }
    }
  }

  ngOnInit()
  {
    this.renderResponse();
  }
}
