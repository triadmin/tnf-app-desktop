import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';

@Component({
  selector: 'list-with-headers-text',
  template: `
    <ion-list>
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
      <div *ngFor="let answer of question.answers; let parentIndex = index">
        <ion-list-header *ngIf="answer.SurveyAnswersExtra == 'header'">
          {{ answer.SurveyAnswersText }}
        </ion-list-header>
        <ion-item *ngIf="answer.SurveyAnswersExtra != 'header'">
          {{ answer.SurveyAnswersText }}

          <input type="number" class="list-input" [(ngModel)]="response[parentIndex]" placeholder="Count" (keyup)="saveResponse($event, answer)" item-right />
        </ion-item>
      </div>
    </ion-list>
  `
})

export class ListWithHeadersTextComponent
{
  @Input() question: any;
  response: any = [];

  constructor( private surveyService: SurveyService ) {
    this.surveyService = surveyService;
  }

  saveResponse( event, answer )
  {
    if ( event.target.value > 0 )
    {
      answer.SurveyAnswersValue = event.target.value;
      this.surveyService.saveResponse( answer, 'list-with-headers-text' ).subscribe(data => {
      });
    }
  }

  renderResponse()
  {
    if ( this.question.Responses.length > 0 )
    {
      var responseObj = this.question.Responses;
      var answersObj = this.question.answers;
      for ( var i = 0; i <= answersObj.length - 1; i++ )
      {
        this.response[i] = '';
        for ( var j = 0; j <= responseObj.length - 1; j++ )
        {
          // Do we have a response for this answer?
          if ( responseObj[j].SurveyResponsesAnswersId === answersObj[i].SurveyAnswersId )
          {
            this.response[i] = responseObj[j].SurveyResponsesValue;
          }
        }
      }
    }
  }

  ngOnInit()
  {
    this.renderResponse();
  }
}
