import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';

@Component({
  selector: 'checkbox',
  template: `
    <ion-list>
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
      <ion-item *ngFor="let answer of question.answers">
        <ion-label>{{ answer.SurveyAnswersText }}</ion-label>
        <ion-checkbox green [(ngModel)]="answer.checked" (click)="saveResponse(answer)"></ion-checkbox>
      </ion-item>
    </ion-list>
  `
})

export class CheckboxComponent
{
  @Input() question: any;
  response: any;

  constructor( private surveyService: SurveyService ) {
    this.surveyService = surveyService;
  }

  saveResponse( answer )
  {
    answer.SurveyAnswersValue = '';
    this.surveyService.saveResponse( answer, 'checkbox' ).subscribe(data => {});
  }

  renderResponse()
  {
    // Look for any reponses.
    if ( this.question.Responses.length > 0 )
    {
      // Loop over answers.  Is an answer has a response, mark it 'checked'.
      var responseObj = this.question.Responses;
      var answersObj = this.question.answers;

      for ( var i = 0; i <= answersObj.length - 1; i++ )
      {
        for ( var j = 0; j <= responseObj.length - 1; j++ )
        {
          // Do we have a response for this answer?
          if ( responseObj[j].SurveyResponsesAnswersId === answersObj[i].SurveyAnswersId )
          {
              answersObj[i].checked = true;
          }
        }
      }
    }
  }

  ngOnInit()
  {
    this.renderResponse();
  }
}
