import { Component, ViewChild, ElementRef } from '@angular/core';
import { App, NavController, NavParams, Alert, Events, PopoverController, ModalController } from 'ionic-angular';
import { GoogleAnalytics } from 'ionic-native';

import { Constants } from '../../providers/config/config';
import { ReportService } from '../../providers/report-service/report-service';
import { SurveyService } from '../../providers/survey-service/survey-service';
import { PlacesService } from '../../providers/places-service/places-service';
import { SurveyPage } from '../survey/survey';
import { ReportsPage } from '../reports/reports';
import { QuarterPopoverPage } from './components/quarter-popover';
import { UpdateAccountPage } from '../account/updateaccount';

// Chart types
import { DonutComponent } from './chart_types/donut';
import { BarComponent } from './chart_types/bar';
import { BarSimpleComponent } from './chart_types/bar-simple';
import { ActivityDurationReportComponent } from './chart_types/activity-duration';

// ToDo Component
import { ToDosDashboardComponent } from '../../pages/activity/components/todos-dashboard.component';

@Component({
  templateUrl: 'dashboard.html'
})

export class DashboardPage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('chart1') chart1: BarSimpleComponent;
  @ViewChild('chart2') chart2: DonutComponent;
  @ViewChild('chart3') chart3: BarSimpleComponent;
  @ViewChild('chart4') chart4: ActivityDurationReportComponent;
  @ViewChild('chart5') chart5: BarSimpleComponent;
  @ViewChild('chart6') chart6: BarSimpleComponent;
  @ViewChild('todos') todos: ToDosDashboardComponent;

  places: any;
  selectedQuarter: any = {};
  quarters: any;
  showDatePickerIcon: boolean = false;
  showIntroPanel: boolean = false;
  userFirstName: string = '';
  platformWidth: number = Constants.PLATFORM_WIDTH;
  entryCoordinates: any;
  user: any = Constants.USER;

  constructor(
    private nav: NavController,
    private modalCtrl: ModalController,
    private popoverCtrl: PopoverController,
    private navParams: NavParams,
    private reportService: ReportService,
    private surveyService: SurveyService,
    private placesService: PlacesService,
    private events: Events
  ) {
    this.events.subscribe('quarters:select', ( quarter ) => {
      this.selectedQuarter = quarter;
      this.refreshChartData();
    });

    GoogleAnalytics.trackView('Dashboard');
  }

  newEntry()
  {
    this.nav.push(SurveyPage);
  }

  viewReports()
  {
    this.nav.push(ReportsPage);
  }

  viewAccountPage()
  {
    this.nav.push(UpdateAccountPage);
  }

  getSurveyAttemptQuarters()
  {
    this.surveyService.getSurveyAttemptQuarters().subscribe(data => {
      this.quarters = data.json().data;
      Constants.DATA_QUARTERS = this.quarters;

      if ( this.quarters.length > 1 )
      {
        this.showDatePickerIcon = true;
      }

      // Set selectedDateObj as last obj in dateRange
      this.selectedQuarter = this.quarters[0];

      // Refresh charts
      this.refreshChartData();
    });
  }

  presentQuarterPopover()
  {
    let popover = this.popoverCtrl.create(QuarterPopoverPage, {
      quarters: this.quarters
    });
    popover.present();
  }

  refreshChartData()
  {
    this.chart1.refreshChartData( this.selectedQuarter );
    this.chart2.refreshChartData( this.selectedQuarter );
    this.chart3.refreshChartData( this.selectedQuarter );
    this.chart4.refreshChartData( this.selectedQuarter );
    this.chart5.refreshChartData( this.selectedQuarter );
    this.chart6.refreshChartData( this.selectedQuarter );

    // See if we even have any survey attempts.
    this.surveyService.getSurveyAttempts('Yes', 0, 0).subscribe(data => {
      let result = data.json().data;
      if ( result.length === 0 )
      {
        this.showIntroPanel = true;
      } else {
        this.showIntroPanel = false;
        this.populatePlaces();
      }
    });
  }

  populatePlaces()
  {
    this.placesService.getUserPlaces().subscribe(data => {
      var result = data.json().data;

      var tempArray = [];
      var coordArray = [];
      for ( var item in result )
      {
        var itemObj = eval('(' + result[item].SurveyResponsesText + ')');
        if ( itemObj.school !== '' )
        {
          tempArray.push( itemObj.school );
        }
      }

      // Pull out unique values from tempArray and push them to the Constants array.
      var uniquePlaceArray = tempArray.sort().filter(function(item, pos, ary) {
        return !pos || item != ary[pos - 1];
      });

      Constants.PLACES = uniquePlaceArray;
    });
  }

  ionViewWillEnter()
  {
    // Get new ToDo's
    this.todos.refreshToDoList();
    
    if ( Constants.REFRESH_DASHBOARD === true )
    {
      // Get survey attempt date range
      // and refresh chart data
      this.getSurveyAttemptQuarters();

      // TODO: See about converting this to an event.
      Constants.REFRESH_DASHBOARD = false;
    }

    // Make sure we show the activity list menu.
    this.events.publish('dashboard:view');
  }
}
