import { Component, Input } from '@angular/core';
import { ReportService } from '../../../providers/report-service/report-service';

@Component({
  selector: 'bar-simple',
  template: `
    <div class="message" *ngIf="!haveData">
      <ion-row>
        <ion-col width-20><ion-icon ios="ios-clipboard-outline" md="md-clipboard"></ion-icon></ion-col>
        <ion-col width-80>You have no data for<br />{{ quarter.period }}.</ion-col>
      </ion-row>
    </div>
    <div *ngIf="haveData">
      <div *ngFor="let data of chartArray; let i = index">
        <span class="muted">{{ data.label }}</span>
        <div class="bar-simple-container">
          <div class="bar" [style.width]="data.percentage + '%'">{{ data.count }}</div>
        </div>
      </div>
    </div>
  `
})

export class BarSimpleComponent
{
  @Input() questionIdentifier: any;
  @Input() reportType: any;
  @Input() maxResults: number = 0;
  @Input() dateStart: any;
  @Input() dateEnd: any;
  @Input() dataElement: string = '';
  chartLabels: any;
  chartData: any;
  chartArray: any = [];
  haveData: boolean = true;
  quarter: any;

  constructor( private reportService: ReportService ) {
    this.reportService = reportService;
  }

  getData( questionIdentifier, reportType )
  {
    var chartLabels = [];
    var chartData = [];

    this.reportService.getReportData( questionIdentifier, this.quarter ).subscribe(data => {
      var chartObj = this.reportService.formatReportData( data.json().data, reportType, this.maxResults );

      if ( this.dataElement !== '' )
      {
        chartLabels = chartObj.chartLabels[ this.dataElement ];
        chartData = chartObj.chartData[ this.dataElement ];
      } else {
        chartLabels = chartObj.chartLabels;
        chartData = chartObj.chartData;
      }

      if ( chartData.length !== 0 )
      {
        this.chartArray = [];
        var chartArray = [];
        for ( var item in chartData )
        {
          var obj = {
            'label': chartLabels[item],
            'count': chartData[item]
          };
          chartArray.push( obj );
        }

        chartArray.sort(function(a, b){
          return b.count - a.count;
        });

        var maxValue = chartArray[0].count;
        for ( var item in chartArray )
        {
          var percentage = ( chartArray[item].count / maxValue ) * 100;
          var newObj = {
            'label': chartArray[item].label,
            'count': chartArray[item].count,
            'percentage': percentage
          };
          this.chartArray.push( newObj );
        }
      }

      if ( chartData.length === 0 )
      {
        this.haveData = false;
      } else {
        this.haveData = true;
      }
    });
  }

  // Reply on parent to tell us when to refresh the data.
  refreshChartData( quarter )
  {
    this.quarter = quarter;
    this.getData( this.questionIdentifier, this.reportType );
  }
}
