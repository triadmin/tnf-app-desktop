import { Component, Input } from '@angular/core';
import { ReportService } from '../../../providers/report-service/report-service';

@Component({
  selector: 'bar',
  template: `
    <!--
    <base-chart class="chart"
     [attr.datasets]="barChartData"
     [attr.labels]="barChartLabels"
     [attr.options]="barChartOptions"
     [attr.legend]="barChartLegend"
     [attr.chartType]="chartType"
     (chartHover)="chartHovered($event)"
     (chartClick)="chartClicked($event)"></base-chart>
     -->
  `
})

export class BarComponent
{
  barChartLabels: any = [];
  barChartData: any = [];
  chartType: string = 'bar';
  barChartOptions: any = [];
  barChartLegend: boolean = false;

  constructor( private reportService: ReportService ) {
    this.reportService = reportService;
    this.barChartData = [
      {data: [65, 59, 80, 81, 56, 55, 40]},
      {data: [28, 48, 40, 19, 86, 27, 90]}
    ];
  }

  chartClicked(e) {
    //console.log(e);
  }

  chartHovered(e) {
    //console.log(e);
  }

  ngAfterViewInit()
  {
    // This is where we get our data from the server and format it.
    this.barChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    this.barChartOptions = {
      scaleShowVerticalLines: false,
      responsive: true,
      legend: {
        display: false
      }
    };
  }
}
