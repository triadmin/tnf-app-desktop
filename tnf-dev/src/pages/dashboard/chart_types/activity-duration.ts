import { Component, Input } from '@angular/core';
import { ReportService } from '../../../providers/report-service/report-service';

@Component({
  selector: 'activity-duration-report',
  template: `
    <div class="message" *ngIf="!haveData">
      <ion-row>
        <ion-col width-20><ion-icon ios="ios-clipboard-outline" md="md-clipboard"></ion-icon></ion-col>
        <ion-col width-80>You have no data for<br />{{ quarter.period }}.</ion-col>
      </ion-row>
    </div>
    <div *ngIf="haveData">
      <h1 class="text-report-value" primary>{{ averageHours }}<span>hr</span> {{ averageMinutes }}<span>min</span></h1>
      <p class="muted text-center">TOTAL ACTIVITY TIME {{ totalHours }}hr {{ totalMinutes }}min</p>
    </div>
  `
})

export class ActivityDurationReportComponent
{
  @Input() questionIdentifier: any;
  @Input() reportType: any;
  averageHours: any;
  averageMinutes: any;
  totalHours: any;
  totalMinutes: any;
  haveData: boolean = true;
  quarter: any;

  constructor( private reportService: ReportService ) {
    this.reportService = reportService;
  }

  getData( questionIdentifier, reportType )
  {
    this.reportService.getReportData( questionIdentifier, this.quarter ).subscribe(data => {
      if ( data.json().data.length === 0 )
      {
        this.haveData = false;
      } else {
        this.haveData = true;
        var chartObj = this.reportService.formatReportData( data.json().data, reportType, null );

        if ( Object.getOwnPropertyNames(chartObj).length !== 0 )
        {
          this.averageHours = chartObj.averageHours;
          this.averageMinutes = chartObj.averageMinutes;
          this.totalHours = chartObj.totalHours;
          this.totalMinutes = chartObj.totalMinutes;
        }
      }
    });
  }

  // Reply on parent to tell us when to refresh the data.
  refreshChartData( quarter )
  {
    this.quarter = quarter;
    this.getData( this.questionIdentifier, this.reportType );
  }
}
