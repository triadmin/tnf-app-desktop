import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, Events } from 'ionic-angular';

@Component({
  template: `
    <ion-list>
      <ion-list-header class="header-sm">Select Quarter</ion-list-header>
      <button ion-item *ngFor="let quarter of quarters" (click)='select(quarter)'>
        {{ quarter.period }}
      </button>
    </ion-list>
  `
})
export class QuarterPopoverPage {
  quarters: any = [];

  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public events: Events
  )
  {
    // navParams contain our array of available dates.
    this.quarters = navParams.data.quarters;
  }

  select( quarter ) {
    this.events.publish('quarters:select', quarter);
    this.viewCtrl.dismiss();
  }
}
