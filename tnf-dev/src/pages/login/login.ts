import { Component } from '@angular/core';
import { App, Events, NavController, MenuController, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { GoogleAnalytics } from 'ionic-native';

import { LoginService } from '../../providers/login-service/login-service';
import { AccountService } from '../../providers/account-service/account-service';
import { Constants } from '../../providers/config/config';
import { DashboardPage } from '../dashboard/dashboard';
import { CreateAccountPage } from '../account/createaccount';

@Component({
  templateUrl: 'login.html'
})
export class LoginPage {
  showLoginForm: boolean = false;
  showForgotPasswordForm: boolean = false;
  loginError: boolean = false;
  findPasswordError: boolean = false;
  findPasswordSuccess: boolean = false;
  pageTitle: string = 'Please Login';

  loginForm: any;
  forgotPasswordForm: any;

  email: string = '';
  password: string = '';
  fpEmail: string = '';

  constructor(
    private nav: NavController,
    private menuCtrl: MenuController,
    private app: App,
    private events: Events,
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private loginService: LoginService
  )
  {
    this.loginForm = formBuilder.group({
        'email': ['', Validators.required],
        'password': ['', Validators.required]
    });

    this.forgotPasswordForm = formBuilder.group({
      'fpEmail': ['', Validators.required]
    });

    this.email = this.loginForm.controls['email'];
    this.password = this.loginForm.controls['password'];
    this.fpEmail = this.forgotPasswordForm.controls['fpEmail'];

    GoogleAnalytics.trackView('Login');
    this.showLoginForm = true;
  }

  submitLoginForm( loginData )
  {
    this.loginService.tryLogin( loginData.email, loginData.password ).subscribe(data => {
      let result = data.json();

      let access_token = result.data.access_token;
      if ( access_token )
      {
        // Save access token in database.
        this.loginService.saveCredentials( access_token ).then((data) => {
        }, (error) => {
          //console.log('ERROR: ', error);
        });

        // Save access token in Constants.
        Constants.ACCESS_TOKEN = access_token;

        // Get user record.
        this.accountService.getUserRecord().subscribe((data) => {
          Constants.USER = data.json().data;

          // Set the root page.
          this.nav.setRoot(DashboardPage);

          // Publish the login event
          this.events.publish('user:login');
        });

        // Get Region data
        this.accountService.getRegionData().subscribe((data) => {
          Constants.REGION_DATA = JSON.parse( data.json().data );
        });

        // Get all TNF user records
        this.accountService.getTnfs().subscribe((data) => {
          Constants.TNF_USERS = data.json().data;
        });

        GoogleAnalytics.trackEvent('Account', 'login', 'Log Into Account');
      } else {
        this.loginError = true;
        GoogleAnalytics.trackEvent('Account', 'login-fail', 'Failed Login Attempt');
      }
    });
  }

  submitForgotPasswordForm( emailData )
  {
    let email = emailData.fpEmail;
    if ( email.length > 0 )
    {
      this.loginService.resetPassword( email ).subscribe(data => {
        let result = data.json();
        if ( result.data.error )
        {
          // Show error message
          this.findPasswordError = true;
        } else {
          // Show success message
          this.findPasswordSuccess = true;
          this.findPasswordError = false;
        }
        GoogleAnalytics.trackEvent('Account', 'forgot-password', 'Forgot Password Retrieval');
      });
    }
  }

  toggleForgotPasswordForm( state )
  {
    if ( state === 'show' )
    {
      this.showLoginForm = false;
      this.showForgotPasswordForm = true;
      this.pageTitle = 'Reset Your Password';
    } else {
      this.showLoginForm = true;
      this.showForgotPasswordForm = false;
      this.pageTitle = 'Please Login';
    }
  }

  createNewAccount()
  {
    this.nav.push(CreateAccountPage);
  }
}
