import { Component, ViewChild, ElementRef } from '@angular/core';
import { App, ViewController, PopoverController, NavParams, Events } from 'ionic-angular';
import { GoogleAnalytics, Printer, PrintOptions } from 'ionic-native';

import { Constants } from '../../providers/config/config';
import { ReportDataStore } from '../../providers/report-service/report-datastore-service';

import { ReportService } from '../../providers/report-service/report-service';
import { PrintService } from '../../providers/print-service/print-service';
import { MapActivityPopoverPage } from './components/map-activity-popover';

declare var window: any;

@Component({
  templateUrl: 'map-report.html'
})

export class MapReportPage
{
  @ViewChild('reportDom') reportDom: ElementRef;

  user: any = Constants.USER;
  data: any;
  coordObj: any = [];
  regionCoords: any;
  selectedQuarter: any;
  entryCoords: any;
  markerData: any = [];
  mapHeight: number;
  mapWidth: number;
  mapContainerId: string;
  pointScale: number = 5;
  selectedRegion: any;
  activities: any;
  activityPopover: any;

  constructor(
    private viewCtrl: ViewController,
    private params: NavParams,
    private reportService: ReportService,
    private popoverCtrl: PopoverController,
    private printService: PrintService,
    private events: Events
  )
  {

    this.selectedRegion = ReportDataStore.REPORT_FILTER_OBJECT.selectedRegion;
    this.regionCoords = Constants.REGION_DATA[0][this.selectedRegion].coords;
    this.activities = ReportDataStore.REPORT_ACTIVITIES;
    this.mapContainerId = this.selectedRegion.replace(' ', '-') + '-map';

    this.filterActivities();
    GoogleAnalytics.trackView('Region Map');

    this.events.subscribe('reports:updatemap', ( mapObj ) => {
      this.selectedRegion = ReportDataStore.REPORT_FILTER_OBJECT.selectedRegion;
      this.regionCoords = Constants.REGION_DATA[0][this.selectedRegion].coords;
      this.activities = ReportDataStore.REPORT_ACTIVITIES;

      this.mapContainerId = this.selectedRegion.replace(' ', '-') + '-map';

      this.filterActivities();
    });
  }

  filterActivities()
  {
    this.activities = this.reportService.filterReportData( ReportDataStore.REPORT_ACTIVITIES, ReportDataStore.REPORT_FILTER_OBJECT );
    this.populateMapCoordinates();
  }

  populateMapCoordinates()
  {
    var coordArray = [];
    this.activities.forEach(function ( activity ) {
      if ( activity.Location.LocationJSON.length > 0 )
      {
        var itemObj = eval('(' + activity.Location.LocationJSON + ')');
        if ( typeof itemObj.coords !== 'undefined' )
        {
          for ( var place in itemObj.coords )
          {
            // region could be county, district, school.
            // Not sure we care right now.
            for ( var coord in itemObj.coords[place] )
            {
              let coordPlaceObj = {
                coords: itemObj.coords[place][coord],
                activity: activity
              }
              coordArray.push( coordPlaceObj );
            }
          }
        }
      }
    });

    this.entryCoords = coordArray;
    this.createCoordObj();
    this.createMarkers();
  }

  createCoordObj()
  {
    let coordCountArray = [];
    coordCountArray.length = 0;

    let coordObj = {
      lat: 0,
      lng: 0,
      count: 0,
      activities: []
    };
    for ( var a = 0; a <= this.entryCoords.length - 1; a++ )
    {
      // Round coords to 3 decimals.
      let coords = this.entryCoords[a].coords;
      let activity = this.entryCoords[a].activity;

      let lat = coords.lat.toFixed(2);
      let lng = coords.lng.toFixed(2);

      if ( a === 0 )
      {
        // Push dummy record into the array for our
        // comparison loop that follows.
        coordCountArray.push( coordObj );
      }

      for ( var b = 0; b <= coordCountArray.length - 1; b++ )
      {
        var found = false;
        if (
          typeof coordCountArray[b] !== 'undefined' &&
          lat === coordCountArray[b].lat &&
          lng === coordCountArray[b].lng
        )
        {
          found = true;
          // Increment count
          var count = parseInt(coordCountArray[b].count + 1);
          coordCountArray[b].count = count;
          coordCountArray[b].activities.push( activity );
          break;
        }
      }

      if ( found === false )
      {
        coordObj = {
          lat: lat,
          lng: lng,
          count: 1,
          activities: [
            activity
          ]
        }
        coordCountArray.push( coordObj );
      }
    }
    this.coordObj = coordCountArray;
  }

  createMarkers()
  {
    var scale = this.pointScale;

    var heightTop: number = this.regionCoords.height.lattop;
    var heightBottom: number = this.regionCoords.height.latbottom;
    var widthLeft: number = -this.regionCoords.width.lngleft;
    var widthRight: number = -this.regionCoords.width.lngright;

    var heightDif = heightTop - heightBottom;
    var widthDif = widthLeft - widthRight;

    let markerData = [];
    markerData.length = 0;
    this.coordObj.forEach(function ( coord ) {
      // Find the difference between top lat and this lat.
      var lat: number = coord.lat;
      var lng: number = coord.lng;
      if ( lat !== 0 && lng !== 0 )
      {
        var difH = heightTop - lat;
        var percentageHeigthDif = ( difH / heightDif ) * 100;

        var difW = widthLeft - -lng;
        var percentageWidthDif = ( difW / widthDif ) * 100;
        var markerObj = {
          'top': percentageHeigthDif,
          'left': percentageWidthDif,
          'size': coord.count * scale,
          'count': coord.count,
          'activities': coord.activities
        };
        markerData.push( markerObj );
      }
    });
    this.markerData = markerData;
  }

  presentActivityPopover( activities, ev )
  {
    this.activityPopover = this.popoverCtrl.create( MapActivityPopoverPage, {
      activities: activities
    });
    this.activityPopover.present({
      ev: ev
    });
  }

  printReport()
  {
    let html = this.reportDom.nativeElement.innerHTML;
    this.printService.printPage( html );
  }

  ionViewDidEnter()
  {
    this.mapWidth = document.getElementById(this.mapContainerId).offsetWidth;
    this.mapHeight = document.getElementById(this.mapContainerId).offsetHeight;
  }

  ngAfterViewInit()
  {

  }
}
