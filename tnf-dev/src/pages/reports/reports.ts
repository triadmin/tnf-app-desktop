import { Component, ViewChildren, ViewChild, ElementRef } from '@angular/core';
import { App, NavController, NavParams, Events, ModalController, ToastController, LoadingController } from 'ionic-angular';
import { GoogleAnalytics } from 'ionic-native';

import { Constants } from '../../providers/config/config';
import { ReportDataStore } from '../../providers/report-service/report-datastore-service';

import { ReportService } from '../../providers/report-service/report-service';
import { HelperService } from '../../providers/helper-service/helper-service';
import { PrintService } from '../../providers/print-service/print-service';

// Components
import { ActivityBoxComponent } from './components/activity-box.component';

// Pages
import { MapReportPage } from './map-report';
import { FrequencyReportPage } from './frequency-report';

@Component({
  templateUrl: 'reports.html'
})

export class ReportsPage {
  @ViewChildren('activities') filteredActivities: any;
  @ViewChild('reportDom') reportDom: ElementRef;

  filteredActivitiesArray: any = [];
  activityData: any = [];
  totalActivities: number = 0;
  selectedActivities: any = [];
  filterObj: any = ReportDataStore.REPORT_FILTER_OBJECT;
  loading: any;
  loadingToast: any;
  entryCoordinates: any;
  mapObj: any = {};
  dataLoaded: boolean = false;

  constructor(
    private nav: NavController,
    private events: Events,
    private reportService: ReportService,
    private helperService: HelperService,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    private printService: PrintService
  ) {
    this.events.subscribe('reports:filteractivities', ( filterObjFromControl ) => {
      this.filterObj = filterObjFromControl;
      if ( filterObjFromControl.selectedQuarter.period !== ReportDataStore.REPORT_PERIOD )
      {
        this.activityData.length = 0;
        ReportDataStore.REPORT_PERIOD = filterObjFromControl.selectedQuarter.period;
        this.getData( filterObjFromControl.selectedQuarter );
      } else {
        // When we filter, get a new array of our filtered
        // activities that we can user for reporting.
        setTimeout(() => {
          this.activityData = ReportDataStore.REPORT_ACTIVITIES;
          this.createFilteredActivitiesArray();
        }, 500);
      }
    });

    GoogleAnalytics.trackView('Reports');
  }

  getData( quarter )
  {
    this.dataLoaded = false;
    this.presentLoadingToast();
    this.reportService.getAttemptData( quarter ).subscribe(data => {
      this.dataLoaded = true;
      this.formatData( data.json().data );
      this.dismissLoadingToast();
    });
  }

  formatData( data )
  {
    var newArray = [];
    let helperService = this.helperService;
    data.forEach(function ( activity ) {
      // Check for missing title
      if ( activity.SurveyAttemptsTitle === null || activity.SurveyAttemptsTitle.length === 0 )
      {
        activity.SurveyAttemptsTitle = '[No Title]';
      }

      // Meta data information
      activity = helperService.addMetaDataFlags(activity);

      activity.Location = {
        'Counties': [],
        'Districts': [],
        'LocationObject': [],
        'LocationJSON': ''
      };
      activity.Comments = '';
      activity.Duration = '';
      activity.TechnicalAssistanceMode = '';
      activity.TypeOfActivity = [];
      activity.Audience = [];
      activity.Deliverables = [];

      for( var response in activity.Responses )
      {
        var resp = activity.Responses[response];
        var qIdentifier = resp.SurveyQuestionsIdentifier;

        if ( qIdentifier == 'TechnicalAssistanceMode')
        {
          activity.TechnicalAssistanceMode = resp.SurveyResponsesText;
        }
        if ( qIdentifier == 'Comments')
        {
          activity.Comments = resp.SurveyResponsesText;
        }
        if ( qIdentifier == 'AreaInvolved' )
        {
          // Parse SurveyResponseText JSON
           try {
             activity.Location.LocationJSON = resp.SurveyResponsesText;
             var respObj = JSON.parse( resp.SurveyResponsesText );
             if ( respObj.counties && respObj.counties.length > 0 )
             {
               activity.Location.Counties = respObj.counties;
             }
             if ( respObj.districts && respObj.districts.length > 0 )
             {
               activity.Location.Districts = respObj.districts;
             }
             activity.Location.LocationObject = respObj;
           } catch (e) {
           }
        }
        if ( qIdentifier == 'DurationOfActivity' )
        {
          activity.Duration = resp.SurveyResponsesText;
        }
        if ( qIdentifier == 'Deliverables' )
        {
          activity.Deliverables.push( resp.SurveyResponsesText );
        }
        if ( qIdentifier == 'TypeOfActivity' )
        {
          activity.TypeOfActivity.push( resp.SurveyResponsesText );
        }
        if ( qIdentifier == 'Audience' )
        {
          activity.Audience.push( resp.SurveyResponsesText );
        }
      }
      newArray.push( activity );
    });

    this.activityData = newArray;

    // Make filteredActivitiesArray
    this.filteredActivitiesArray.length = 0;
    for ( let item in this.activityData )
    {
      this.filteredActivitiesArray.push( this.activityData[item] );
      // Push to ReportDataStore
      ReportDataStore.REPORT_ACTIVITIES.push( this.activityData[item] );
    }
    this.createFilteredActivitiesArray();
    this.updateReportElements();
  }

  // Create an array of just our filtered activities
  // that we can use for the report elements.
  createFilteredActivitiesArray()
  {
    this.filteredActivitiesArray = this.reportService.filterReportData( this.activityData, this.filterObj );

    this.updateReportElements();
  }

  updateReportElements()
  {
    let reportElementData = {
      filteredActivities: this.filteredActivitiesArray,
      totalActivityCount: this.activityData.length
    };
    this.events.publish('reports:updatereportelements', reportElementData);
  }

  presentLoadingToast()
  {
    this.loadingToast = this.toastCtrl.create({
      message: 'Getting activities ... just a moment, please.',
      position: 'bottom',
      cssClass: 'toast-loading'
    });

    this.loadingToast.present();
  }

  dismissLoadingToast()
  {
    this.loadingToast.dismiss();
  }

  presentMapPage()
  {
    this.nav.push( MapReportPage );
  }

  presentFrequencyPage()
  {
    this.nav.push( FrequencyReportPage, {selectedQuarter: this.filterObj.selectedQuarter} );
  }

  printReport()
  {
    let html = this.reportDom.nativeElement.innerHTML;
    this.printService.printPage( html );
  }

  ionViewWillEnter()
  {
    this.events.publish('reports:view');
  }

  ionViewWillLeave() {
    this.events.publish('reports:leave');
  }
}
