import { Component, ViewChild, ElementRef } from '@angular/core';
import { App, ViewController, NavParams } from 'ionic-angular';
import { GoogleAnalytics } from 'ionic-native';

import { Constants } from '../../providers/config/config';
import { ReportService } from '../../providers/report-service/report-service';
import { PrintService } from '../../providers/print-service/print-service';

// Chart types
import { BarSimpleComponent } from '../../pages/dashboard/chart_types/bar-simple';

@Component({
  templateUrl: 'frequency-report.html'
})

export class FrequencyReportPage
{
  @ViewChild('freqChart1') freqChart1: BarSimpleComponent;
  @ViewChild('freqChart2') freqChart2: BarSimpleComponent;
  @ViewChild('freqChart3') freqChart3: BarSimpleComponent;
  @ViewChild('reportDom') reportDom: ElementRef;

  frequencyType: string = 'activityType';
  selectedQuarter: any = {};

  constructor(
    private viewCtrl: ViewController,
    private params: NavParams,
    private reportService: ReportService,
    private printService: PrintService
  )
  {
    GoogleAnalytics.trackView('Frequency Reports');
  }

  showChart( chartType )
  {
    this.frequencyType = chartType;
  }

  printReport()
  {
    let html = this.reportDom.nativeElement.innerHTML;
    this.printService.printPage( html );
  }

  ionViewWillEnter()
  {
    this.selectedQuarter = this.params.get( 'selectedQuarter' );
    this.freqChart1.refreshChartData( this.selectedQuarter );
    this.freqChart2.refreshChartData( this.selectedQuarter );
    this.freqChart3.refreshChartData( this.selectedQuarter );
    this.showChart( this.frequencyType );
  }
}
