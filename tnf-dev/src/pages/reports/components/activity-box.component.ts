import {Component, Input, Output} from '@angular/core';
import { Events } from 'ionic-angular';

@Component({
  selector: 'activity-box',
  templateUrl: 'activity-box.component.html'
})

export class ActivityBoxComponent
{
  @Input() public activity: any;
  @Input() public selectedQuarter: any;

  constructor(
    private events: Events
  )
  {}

  showActivityPage(activity)
  {
    this.events.publish('activity:view', activity);
  }

  filterActivities(filterType, filterValue)
  {
    let filterObj = {
      selectedRegion: '',
      selectedCounty: '',
      selectedDistrict: '',
      selectedActivities: [],
      selectedAudiences: [],
      selectedDeliverables: [],
      selectedQuarter: this.selectedQuarter,
      filterType: filterType
    };

    if ( filterType === 'region' )
    {
      filterObj.selectedRegion = filterValue[0];
    }
    if ( filterType === 'county' )
    {
      filterObj.selectedRegion = filterValue[0];
      filterObj.selectedCounty = filterValue[1];
    }
    if ( filterType === 'district' )
    {
      filterObj.selectedRegion = filterValue[0];
      filterObj.selectedDistrict = filterValue[1];
    }

    this.events.publish( 'reports:filteractivities-from-activity-box', filterObj );
  }
}
