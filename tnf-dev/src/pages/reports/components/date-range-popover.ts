import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, PopoverController } from 'ionic-angular';

@Component({
  template: `
    <ion-list>
      <ion-list-header>Select a Quarter</ion-list-header>
      <button ion-item>Quarter 1 2016</button>
      <button ion-item>Quarter 2 2016</button>
      <button ion-item>Quarter 3 2016</button>
      <button ion-item>Quarter 4 2016</button>
    </ion-list>
    <ion-list>
      <ion-list-header>Custom Date Range</ion-list-header>
      <ion-item>
        <ion-label>From</ion-label>
        <ion-input [(ngModel)]="dateFrom" (keyup)="formatDateInput($event)" type="text" placeholder="mm/dd/yyyy"></ion-input>
      </ion-item>
      <ion-item>
        <ion-label>To</ion-label>
        <ion-input [(ngModel)]="dateTo" (keyup)="formatDateInput($event)" type="text" placeholder="mm/dd/yyyy"></ion-input>
      </ion-item>
    </ion-list>
  `
})

export class DateRangePopoverPage
{
  dateFrom: string = '';
  dateTo: string = '';

  constructor()
  {

  }

  formatDateInput(event)
  {
    var date = event.target.value;

    if ( date.length == 2 || date.length == 5 )
    {
      date += '/';
    }

    this.dateFrom = date;
  }
}
