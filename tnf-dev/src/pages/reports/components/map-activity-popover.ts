import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, Events } from 'ionic-angular';

@Component({
  template: `
    <ion-list>
      <ion-list-header class="header-sm">Activities</ion-list-header>
      <button ion-item *ngFor="let activity of activities" (click)='showActivityPage(activity)'>
        {{ activity.SurveyAttemptsTitle }}
      </button>
    </ion-list>
  `
})
export class MapActivityPopoverPage {
  activities: any = [];

  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public events: Events,
    public navCtrl: NavController
  )
  {
    this.activities = navParams.data.activities;
  }

  showActivityPage(activity)
  {
    this.navCtrl.pop();
    this.events.publish('activity:view', activity);
  }
}
