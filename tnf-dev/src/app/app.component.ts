import { Component, ViewChild } from '@angular/core';
import { Events, MenuController, Nav, Platform, ToastController } from 'ionic-angular';
import { StatusBar, Splashscreen, GoogleAnalytics } from 'ionic-native';

// Pages
import { LoginPage } from '../pages/login/login';
import { ActivityViewPage } from '../pages/activity/activity-view';
import { DashboardPage } from '../pages/dashboard/dashboard';

// Constants
import { Constants } from '../providers/config/config';

// Services
import { AppDataService } from '../providers/app-data-service/app-data-service';
import { AccountService } from '../providers/account-service/account-service';
import { LoginService } from '../providers/login-service/login-service';

// Components
import { ActivityListComponent } from '../pages/activity/components/activity-list.component';
import { ReportFilterControlComponent } from '../pages/reports/components/control.component';
import { ReportWidgetComponent } from '../pages/reports/components/report-widget.component';

@Component({
  templateUrl: 'app.html'
})
export class TnfApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  activeMenu: string = 'blankMenu';

  constructor(
    public menu: MenuController,
    public platform: Platform,
    public events: Events,
    public toastCtrl: ToastController,
    public addDataService: AppDataService,
    public accountService: AccountService,
    public loginService: LoginService
  ) {
    this.initializeApp();
    this.listenToEvents();
  }

  initializeApp() {
    this.rootPage = LoginPage;
  }

  activateMenu( menuId )
  {
    this.activeMenu = menuId;
    this.menu.enable(true, menuId);
  }

  listenToEvents() {
    this.events.subscribe('activity:view', ( surveyAttempt ) => {
      // If activity view page is already on the stack, don't push another
      // copy of it onto the stack.  Just update the data with an event.
      if ( this.nav.getActive().name === 'ActivityViewPage' )
      {
        this.events.publish( 'activity:updateview', surveyAttempt );
      } else {
        this.nav.push(ActivityViewPage, { surveyAttempt: surveyAttempt });
      }
    });

    // Log events
    this.events.subscribe('app:log', ( logData ) => {
      this.presentToast( logData );
    });

    this.events.subscribe('user:login', () => {
      this.activateMenu( 'activityMenu' );
    });

    this.events.subscribe('user:logout', () => {
      this.rootPage = LoginPage;
      Constants.REFRESH_DASHBOARD = true;
      this.activateMenu('blankMenu');
    });

    // Swap menus when viewing reports
    this.events.subscribe('reports:view', () => {
      this.activateMenu( 'reportMenu' );
    });

    // Swap menus when leaving reports
    this.events.subscribe('dashboard:view', () => {
      this.activateMenu( 'activityMenu' );
    });
  }

  presentToast( logData )
  {
    let toast = this.toastCtrl.create({
      message: logData.message,
      duration: 3000,
      position: 'bottom',
      cssClass: 'toast-' + logData.status
    });

    toast.onDidDismiss(() => {
      // We can do something here.
    });

    toast.present();
  }
}
