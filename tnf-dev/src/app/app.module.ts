import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { SQLite, Printer, PrintOptions } from 'ionic-native';

// App component
import { TnfApp } from './app.component';

// Pages
import { CreateAccountPage } from '../pages/account/createaccount';
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { SurveyPage } from '../pages/survey/survey';
import { ReportsPage } from '../pages/reports/reports';
import { ActivityViewPage } from '../pages/activity/activity-view';
import { UpdateAccountPage } from '../pages/account/updateaccount';
import { MapReportPage } from '../pages/reports/map-report';
import { FrequencyReportPage } from '../pages/reports/frequency-report';

// Modals and popovers
import { NotificationsModalPage } from '../pages/activity/components/notifications-modal';
import { QuarterPopoverPage } from '../pages/dashboard/components/quarter-popover';
import { DateRangePopoverPage } from '../pages/reports/components/date-range-popover';
import { MapActivityPopoverPage } from '../pages/reports/components/map-activity-popover';

// Service providers
import { AccountService } from '../providers/account-service/account-service';
import { LoginService } from '../providers/login-service/login-service';
import { ReportService } from '../providers/report-service/report-service';
import { SurveyService } from '../providers/survey-service/survey-service';
import { PlacesService } from '../providers/places-service/places-service';
import { UserService } from '../providers/user-service/user-service';
import { AppDataService } from '../providers/app-data-service/app-data-service';
import { HelperService } from '../providers/helper-service/helper-service';
import { ToDosService } from '../providers/todos-service/todos-service';
import { PrintService } from '../providers/print-service/print-service';

// Other modules.
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { LinkyModule } from 'angular-linky';

// Chart types
import { DonutComponent } from '../pages/dashboard/chart_types/donut';
import { BarComponent } from '../pages/dashboard/chart_types/bar';
import { BarSimpleComponent } from '../pages/dashboard/chart_types/bar-simple';
import { ActivityDurationReportComponent } from '../pages/dashboard/chart_types/activity-duration';

// Question types
import {SurveyAttemptComponent} from '../pages/survey/question_types/survey-attempt';
import {HeadingComponent} from '../pages/survey/question_types/heading';
import {RadioComponent} from '../pages/survey/question_types/radio';
import {TextComponent} from '../pages/survey/question_types/text';
import {TextareafieldComponent} from '../pages/survey/question_types/textarea';
import {CheckboxComponent} from '../pages/survey/question_types/checkbox';
import {DatetimeComponent} from '../pages/survey/question_types/datetime';
import {DatepickerComponent} from '../pages/survey/question_types/datepicker';
import {SelectComponent} from '../pages/survey/question_types/select';
import {SelectmultiComponent} from '../pages/survey/question_types/selectmulti';
import {ListWithHeadersCheckboxComponent} from '../pages/survey/question_types/list-with-headers-checkbox';
import {ListWithHeadersTextComponent} from '../pages/survey/question_types/list-with-headers-text';
import {SelectCountyDistrictSchoolComponent} from '../pages/survey/question_types/select-county-district-school';

// Report components
import {ReportFilterControlComponent} from '../pages/reports/components/control.component';
import {ActivityBoxComponent} from '../pages/reports/components/activity-box.component';
import {ReportWidgetComponent} from '../pages/reports/components/report-widget.component';

// Activity list components
import { ActivityListComponent } from '../pages/activity/components/activity-list.component';

// Activity view components
import { ToDosComponent } from '../pages/activity/components/todos.component';
import { ToDosDashboardComponent } from '../pages/activity/components/todos-dashboard.component';

// Pipes
import { FilterPipeRegion } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeCounty } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeDistrict } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeSearch } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeActivityType } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeDeliverable } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeAudience } from '../pages/reports/pipes/activity-filter.pipes';

import { NewlinePipe } from '../app/pipes/common.pipes';

@NgModule({
  declarations: [
    TnfApp,
    CreateAccountPage,
    LoginPage,
    DashboardPage,
    SurveyPage,
    ReportsPage,
    ActivityViewPage,
    UpdateAccountPage,
    MapReportPage,
    FrequencyReportPage,

    SurveyAttemptComponent,
    HeadingComponent,
    RadioComponent,
    TextComponent,
    TextareafieldComponent,
    CheckboxComponent,
    DatetimeComponent,
    DatepickerComponent,
    SelectComponent,
    SelectmultiComponent,
    ListWithHeadersCheckboxComponent,
    ListWithHeadersTextComponent,
    SelectCountyDistrictSchoolComponent,

    ReportFilterControlComponent,
    ActivityBoxComponent,
    ActivityListComponent,
    ReportWidgetComponent,
    ToDosComponent,
    ToDosDashboardComponent,

    NotificationsModalPage,
    QuarterPopoverPage,
    DateRangePopoverPage,
    MapActivityPopoverPage,

    DonutComponent,
    BarComponent,
    BarSimpleComponent,
    ActivityDurationReportComponent,

    FilterPipeRegion,
    FilterPipeCounty,
    FilterPipeDistrict,
    FilterPipeSearch,
    FilterPipeActivityType,
    FilterPipeDeliverable,
    FilterPipeAudience,
    NewlinePipe
  ],
  imports: [
    IonicModule.forRoot(TnfApp, {
      platforms: {
        ios: {
          scrollAssist: false,
          autoFocusAssist: false
        }
      }
    }),
    ChartsModule,
    FormsModule,
    LinkyModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    TnfApp,
    CreateAccountPage,
    LoginPage,
    DashboardPage,
    MapReportPage,
    SurveyPage,
    ReportsPage,
    ActivityViewPage,
    NotificationsModalPage,
    QuarterPopoverPage,
    UpdateAccountPage,
    MapActivityPopoverPage,
    FrequencyReportPage
  ],
  providers: [
    AccountService,
    LoginService,
    ReportService,
    SurveyService,
    PlacesService,
    UserService,
    AppDataService,
    HelperService,
    ToDosService,
    PrintService,

    FilterPipeRegion,
    FilterPipeCounty,
    FilterPipeDistrict,
    FilterPipeSearch,
    FilterPipeActivityType,
    FilterPipeDeliverable,
    FilterPipeAudience,
    NewlinePipe,

    SQLite,
    {
      provide: ErrorHandler,
      useClass: IonicErrorHandler
    }
  ]
})
export class AppModule {}
