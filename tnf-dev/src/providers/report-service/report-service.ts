import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Constants } from '../../providers/config/config';

// Pipes
import { FilterPipeRegion } from '../../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeCounty } from '../../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeDistrict } from '../../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeSearch } from '../../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeActivityType } from '../../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeDeliverable } from '../../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeAudience } from '../../pages/reports/pipes/activity-filter.pipes';

import 'rxjs/Rx';

@Injectable()
export class ReportService {
  http: Http;
  survey_id: number = Constants.SURVEY_ID;
  server_url: string = Constants.SERVER_URL;

  constructor(
    http: Http,

    private filterPipeSearch: FilterPipeSearch,
    private filterPipeRegion: FilterPipeRegion,
    private filterPipeCounty: FilterPipeCounty,
    private filterPipeDistrict: FilterPipeDistrict,
    private filterPipeAudience: FilterPipeAudience,
    private filterPipeActivityType: FilterPipeActivityType,
    private filterPipeDeliverable: FilterPipeDeliverable
  ) {
    this.http = http;
  }

  // Get data for dashboard summary reports.
  getReportData( questionIdentifier, quarter )
  {
    var dateStart = quarter.period_start;
    var dateEnd = quarter.period_end;
    var url = Constants.SERVER_URL + 'surveyreports/report_data/' + this.survey_id + '/' + questionIdentifier + '?date_start=' + dateStart + '&date_end=' + dateEnd + '&access_token=' + Constants.ACCESS_TOKEN;
    return this.http.get(url);
  }

  // Get all attempt data for full reports.
  getAttemptData( quarter )
  {
    // Get activity entries: survey attempts
    let dateStart = quarter.period_start;
    let dateEnd = quarter.period_end;
    var url = Constants.SERVER_URL + 'survey/survey_attempts_for_user/' + this.survey_id + '?date_start=' + dateStart + '&date_end=' + dateEnd + '&with_responses=Yes&with_follow_ups=Yes&access_token=' + Constants.ACCESS_TOKEN;

    return this.http.get(url);
  }

  filterReportData( activities, filterObj )
  {
    // Start by filtering activityData because it contains all activities.
    let filteredActivitiesArray = this.filterPipeRegion.transform( activities, filterObj.selectedRegion );
    // Then filter filteredActivitiesArray to drill down
    filteredActivitiesArray = this.filterPipeCounty.transform( filteredActivitiesArray, filterObj.selectedCounty );

    filteredActivitiesArray = this.filterPipeDistrict.transform( filteredActivitiesArray, filterObj.selectedDistrict );

    filteredActivitiesArray = this.filterPipeActivityType.transform( filteredActivitiesArray, filterObj.selectedActivities );

    filteredActivitiesArray = this.filterPipeDeliverable.transform( filteredActivitiesArray, filterObj.selectedDeliverables );

    filteredActivitiesArray = this.filterPipeAudience.transform( filteredActivitiesArray, filterObj.selectedAudiences );

    filteredActivitiesArray = this.filterPipeSearch.transform( filteredActivitiesArray, filterObj.searchTerm );

    return filteredActivitiesArray;
  }

  formatReportData( data, reportType, maxResults )
  {
    var reportData;
    switch ( reportType )
    {
      case 'Activity':
        reportData = this.formatActivityReport( data, maxResults );
        break;
      case 'Visit':
        reportData = this.formatVisitReport( data );
        break;
      case 'Site':
        reportData = this.formatSiteReport( data, maxResults );
        break;
      case 'ActivityDuration':
        reportData = this.formatActivityDurationReport( data );
        break;
      case 'Audience':
        reportData = this.formatAudienceReport( data, maxResults );
        break;
      case 'Deliverables':
        reportData = this.formatDeliverablesReport( data, maxResults );
        break;
    }
    return reportData;
  }

  // Most frequent activities.
  formatActivityReport( data, maxResults )
  {
    var newArray = [];
    var chartLabels = [];
    var chartData = [];

    // Convert data object into an array.
    // TODO: this whole chunk of code could be put into a function.
    for ( var item in data )
    {
      newArray.push( data[item].SurveyResponsesText )
    }

    // Count up the unique array values (e.g. Attend Training: 3)
    var uniqueArray = [];
    newArray.forEach(function ( a ) {
      if ( a in uniqueArray ) uniqueArray[a]++;
      else uniqueArray[a] = 1;
    });

    // Turn uniqueArray into something we can sort
    var sortableArray = [];
    for ( var key in uniqueArray )
    {
      var obj = {
        'item': key,
        'count': uniqueArray[key]
      };
      sortableArray.push( obj );
    }

    sortableArray.sort(function(a, b){
      return b.count - a.count;
    });

    // Grab the first 4 values and put them into chart arrays.
    var count = 0;
    sortableArray.forEach(function ( a ) {
      if ( count < maxResults || maxResults === 0 )
      {
        chartLabels.push( a.item );
        chartData.push( a.count );
        count++;
      }
    });

    var chartObj = {
      'chartLabels': chartLabels,
      'chartData': chartData
    }

    return chartObj;
  }

  // One time visits vs. repeat visits.
  formatVisitReport( data )
  {
    var chartLabels = ['One time visits', 'Follow up visits'];
    if ( data.one_time_visits === 0 && data.follow_up_visits === 0 )
    {
      var chartData = [];
    } else {
      var chartData = [ data.one_time_visits, data.follow_up_visits ];
    }
    var chartObj = {
      'chartLabels': chartLabels,
      'chartData': chartData
    }
    return chartObj;
  }

  // Most visited sites.
  formatSiteReport( data, maxResults )
  {
    var newArray = [];
    var chartLabels = [];
    var chartData = [];
    var chartLabelsCounty = [];
    var chartDataCounty = [];
    var chartLabelsDistrict = [];
    var chartDataDistrict = [];

    // Convert data object into an array.
    var counties = [];
    var districts = [];
    var region = 0;
    for ( var item in data )
    {
      var itemObj = eval('(' + data[item].SurveyResponsesText + ')');
      if ( itemObj.counties && itemObj.counties.length > 0 )
      {
        itemObj.counties.forEach( function(a) {
          counties.push(a);
        });
      }

      if ( itemObj.districts && itemObj.districts.length > 0 )
      {
        itemObj.districts.forEach( function(a) {
          districts.push(a);
        });
      }

      if ( itemObj.region === 'Yes' )
      {
        region++;
      }
    }

    var uniqueCountyArray = [];
    counties.forEach(function ( a ) {
      if ( a in uniqueCountyArray ) uniqueCountyArray[a]++;
      else uniqueCountyArray[a] = 1;
    });
    var uniqueDistrictArray = [];
    districts.forEach(function ( a ) {
      if ( a in uniqueDistrictArray ) uniqueDistrictArray[a]++;
      else uniqueDistrictArray[a] = 1;
    });

    // Turn uniqueArrays into something we can sort
    var sortableCountyArray = [];
    for ( var key in uniqueCountyArray )
    {
      var obj = {
        'item': key,
        'count': uniqueCountyArray[key]
      };
      sortableCountyArray.push( obj );
    }

    var sortableDistrictArray = [];
    for ( var key in uniqueDistrictArray )
    {
      var obj = {
        'item': key,
        'count': uniqueDistrictArray[key]
      };
      sortableDistrictArray.push( obj );
    }

    sortableCountyArray.sort(function(a, b){
      return b.count - a.count;
    });

    sortableDistrictArray.sort(function(a, b){
      return b.count - a.count;
    });

    var count = 0;
    sortableCountyArray.forEach(function ( a ) {
      if ( count < maxResults || maxResults === 0 )
      {
        chartLabelsCounty.push( a.item );
        chartDataCounty.push( a.count );
        count++;
      }
    });

    var count = 0;
    sortableDistrictArray.forEach(function ( a ) {
      if ( count < 5 )
      {
        chartLabelsDistrict.push( a.item );
        chartDataDistrict.push( a.count );
        count++;
      }
    });

    var chartObj = {
      'chartLabels': {
        'district': chartLabelsDistrict,
        'county': chartLabelsCounty
      },
      'chartData': {
        'district': chartDataDistrict,
        'county': chartDataCounty
      }
    }

    return chartObj;
  }

  // Average activity duration.
  formatActivityDurationReport( data )
  {
    var minutes: number;
    var minutesTotal: number = 0;
    for ( var item in data )
    {
      var time = data[item].SurveyResponsesText;
      var timeArray = time.split(' ');
      var timeValue: number = timeArray[0];
      if ( timeArray[1] === 'hours' || timeArray[1] === 'hour' )
      {
        minutes = +timeValue * 60;
      } else {
        minutes = +timeValue;
      }
      minutesTotal = minutesTotal + minutes;
    }

    // Get average number of minutes per visit.
    var minutesAverage = minutesTotal / data.length;

    // Format our minutesTotal and minutesHours.
    var totalMinutes = minutesTotal % 60;
    var totalHours = ( minutesTotal - totalMinutes ) / 60;

    var averageMinutes = minutesAverage % 60;
    var averageHours = ( minutesAverage - averageMinutes ) / 60;

    // Returned data needs to be in the form of:
    // 2 hrs 15 minutes
    // Total Activity Time 14 hrs 35 minutes
    var chartObj = {
      'totalHours': totalHours,
      'totalMinutes': totalMinutes,
      'averageHours': averageHours.toFixed(),
      'averageMinutes': averageMinutes.toFixed()
    }
    return chartObj;
  }

  // Most frequest audience types
  formatAudienceReport( data, maxResults )
  {
    var newArray = [];
    var chartLabels = [];
    var chartData = [];

    // Convert data object into an array.
    for ( var item in data )
    {
      newArray.push( [data[item].SurveyResponsesText, data[item].SurveyResponsesValue] );
    }

    // Count up the unique array values (e.g. Attend Training: 3)
    var uniqueArray = [];
    newArray.forEach(function (a) {
      if ( a[0] in uniqueArray ) uniqueArray[a[0]]++;
      else uniqueArray[a[0]] = 1;
    });

    // Turn uniqueArray into something we can sort
    var sortableArray = [];
    for ( var key in uniqueArray )
    {
      var obj = {
        'item': key,
        'count': uniqueArray[key]
      };
      sortableArray.push( obj );
    }

    sortableArray.sort(function(a, b){
      return b.count - a.count;
    });

    var finalArray = [];
    sortableArray.forEach( function( a ) {
      var itemCount: number = 0;

      newArray.forEach(function ( b ) {
        if ( a.item === b[0] )
        {
          itemCount = itemCount + parseInt(b[1]);
        }
      });
      var obj = {
        'item': a.item,
        'count': itemCount
      };
      finalArray.push( obj )
    });

    finalArray.sort(function(a, b){
      return b.count - a.count;
    });

    var count = 0;
    finalArray.forEach( function( a ) {
      if ( count < maxResults || maxResults === 0 )
      {
        chartLabels.push( a.item );
        chartData.push( a.count );
        count++;
      }
    });

    var chartObj = {
      'chartLabels': chartLabels,
      'chartData': chartData
    }

    return chartObj;
  }

  // Most frequent activities.
  formatDeliverablesReport( data, maxResults )
  {
    var newArray = [];
    var chartLabels = [];
    var chartData = [];

    // Convert data object into an array.
    // TODO: this whole chunk of code could be put into a function.
    for ( var item in data )
    {
      newArray.push( data[item].SurveyResponsesText )
    }

    // Count up the unique array values (e.g. Attend Training: 3)
    var uniqueArray = [];
    newArray.forEach(function ( a ) {
      if ( a in uniqueArray ) uniqueArray[a]++;
      else uniqueArray[a] = 1;
    });

    // Turn uniqueArray into something we can sort
    var sortableArray = [];
    for ( var key in uniqueArray )
    {
      var obj = {
        'item': key,
        'count': uniqueArray[key]
      };
      sortableArray.push( obj );
    }

    sortableArray.sort(function(a, b){
      return b.count - a.count;
    });

    // Grab the first 4 values and put them into chart arrays.
    var count = 0;
    sortableArray.forEach(function ( a ) {
      if ( count < maxResults || maxResults === 0 )
      {
        chartLabels.push( a.item );
        chartData.push( a.count );
        count++;
      }
    });

    var chartObj = {
      'chartLabels': chartLabels,
      'chartData': chartData
    }

    return chartObj;
  }
}
