import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Constants } from '../../providers/config/config';
import 'rxjs/Rx';

@Injectable()
export class AccountService {
  http: Http;
  survey_id: number = Constants.SURVEY_ID;
  server_url: string = Constants.SERVER_URL;

  constructor( http: Http ) {
    this.http = http;
  }

  createAccount( user )
  {
    let headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		});
		let options = new RequestOptions({
			headers: headers
		});

    let data = 'first=' + user.firstname + '&last=' + user.lastname + '&email=' + user.email + '&password=' + user.password + '&class=TNF&UsersState=OR&region=' + user.region + '&captcha=research';
    return this.http.post(Constants.SERVER_URL + 'auth/register?access_token=1', data, options);
  }

  getUserRecord()
  {
    return this.http.get(Constants.SERVER_URL + 'auth/user?access_token=' + Constants.ACCESS_TOKEN);
  }

  getRegionData()
  {
    return this.http.get(Constants.SERVER_URL + 'tnfapp/regiondata?access_token=' + Constants.ACCESS_TOKEN);
  }

  updateAccount( user )
  {
    let headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		});
		let options = new RequestOptions({
			headers: headers
		});
    let data = 'UsersFirstName=' + user.UsersFirstName + '&UsersLastName=' + user.UsersLastName + '&UsersEmail=' + user.UsersEmail + '&UsersRegion=' + user.UsersRegion;
    return this.http.post(Constants.SERVER_URL + 'users/update_contact/' + user.UsersId, data, options);
  }

  resetPassword( password, passwordConfirm, userId )
  {
    let headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		});
		let options = new RequestOptions({
			headers: headers
		});
    let data = 'Password=' + password + '&Password_confirmation=' + passwordConfirm;
    return this.http.post(Constants.SERVER_URL + 'users/update_password/' + userId, data, options);
  }

  getTnfs()
  {
    return this.http.get(Constants.SERVER_URL + 'tnfapp/tnfs?access_token=' + Constants.ACCESS_TOKEN);
  }
}
