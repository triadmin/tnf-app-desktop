import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Constants } from '../../providers/config/config';
import 'rxjs/Rx';

@Injectable()
export class LoginService {
  constructor(
    private http: Http
  ) {

    this.http = http;
  }

  openDatabase()
  {
    return Promise.resolve();
  }

  getCredentials()
  {
    return Promise.resolve();
  }

  saveCredentials( access_token )
  {
    return Promise.resolve();
  }

  tryLogin( email, password )
  {
    // Try login at the server using the API.
    let headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		});
		let options = new RequestOptions({
			headers: headers
		});
		let data = 'email=' + email + '&password=' + password;

    return this.http.post(Constants.SERVER_URL + 'auth?access_token=1', data, options);
  }

  resetPassword( email )
  {
    // Try login at the server using the API.
    let headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		});
		let options = new RequestOptions({
			headers: headers
		});
		let data = 'email=' + email;

    return this.http.post(Constants.SERVER_URL + 'auth/reset_password?access_token=1', data, options);
  }

  logout()
  {
    return Promise.resolve();
  }
}
