import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { SQLite } from 'ionic-native';

import { Constants } from '../../providers/config/config';
import 'rxjs/Rx';

@Injectable()
export class AppDataService {

  constructor(
    private http: Http
  )
  {
    this.http = http;
  }

  // Get the print CSS file from <local>/documents
  getPrintCSSFile()
  {
    this.http.get('/documents/style_print.css').subscribe((data) => {
      Constants.PRINT_CSS = data.text();
    });
  }
}
